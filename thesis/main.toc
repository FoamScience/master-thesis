\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Résumé}{iii}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Remerciements}{iv}{section*.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Table des matières}{v}{section*.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Conventions du style}{xi}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Liste des Symboles}{xii}{chapter*.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Liste des Figures}{xvi}{chapter*.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Liste des Tableaux}{xix}{chapter*.9}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Introduction Générale}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Objectifs de ce travail}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Méthodologie de l'étude}{3}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Investigation \& critique des travaux antérieurs}{4}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.1}Manipulation officielle des régions poreuses dans OpenFOAM}{4}{subsection.1.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.2}Solveurs pour la simulation monophasique de réservoir }{4}{subsection.1.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4.3}Une boîte à outils pour l'écoulement biphasique dans un milieux poreux}{5}{subsection.1.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Plan de la thèse}{5}{section.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\@mypartnumtocformat {I}{Analyse \& Développement du réservoir GRDC}}{7}{part.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Présentation \& caractéristiques du champ RDC}{8}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Présentation du champ de Rhoudre Chegga}{8}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Évolution structurale}{8}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Zonation et corrélation réservoir}{9}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}Sédimentologie et continuité du Réservoir}{12}{subsection.2.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}Propriétés et qualité du Réservoir}{12}{subsection.2.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.5}Conclusion}{12}{subsection.2.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Les caractéristiques pétro-physiques du réservoir GRDC:}{13}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Hauteur, Porosité, Saturation \& Perméabilité}{13}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}La relation porosité-perméabilité}{14}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Etude du SCAL \& PVT}{17}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Perméabilité relative}{17}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Normalisation, moyennement et dénormalisation des données de $k_r$ }{17}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.1}La normalisation}{17}{subsubsection.3.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.2}Le Moyennement}{18}{subsubsection.3.1.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.3}Dénormalisation}{19}{subsubsection.3.1.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1.4}Utilisation du logiciel SCAL}{19}{subsubsection.3.1.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Mise à l'échelle des table de saturation (Saturation table Scaling)}{19}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2.1}Terminologie des End-points (points finaux) de saturation}{22}{subsubsection.3.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2.2}Horizontal scaling}{23}{subsubsection.3.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2.3}Vertical Scaling}{24}{subsubsection.3.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Lissage des courbes de la dérivée de $k_r$ pour réduire les problèmes de convergence}{25}{subsection.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Pression capillaire}{26}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Étude PVT}{27}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Les principaux tests du l'étude PVT}{27}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.1.1}Expansion à composition constante (CCE)}{28}{subsubsection.3.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.1.2}Libération différentielle (DL)}{28}{subsubsection.3.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.3.1.3}Test de séparateur (TS)}{30}{subsubsection.3.3.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Commentaire des résultats}{32}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Ajustement des données de libération différentielle aux conditions du séparateur}{32}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Estimation des reserves \& mécanismes de drainage}{35}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Analyse de la production et la pression}{35}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Estimation des réserves}{36}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Méthode volumétrique}{37}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1.1}Détermination des différents paramètres intervenant dans l'équation}{38}{subsubsection.4.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1.2}Utilisation de la méthode Monte Carlo}{38}{subsubsection.4.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}La méthode dynamique (bilan matière)}{39}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2.1}L'équation de bilan matière}{39}{subsubsection.4.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2.2}Forme linéaire de l'équation MBE}{40}{subsubsection.4.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2.3}Version abrégée de l'équation (MBE)}{41}{subsubsection.4.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2.4}Estimation des réserves du GRDC par la méthode de la ligne droite de l’équation MBE}{41}{subsubsection.4.2.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Mécanisme de drainage}{43}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Modèle de simulation \& History matching}{44}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Le modèle de simulation}{44}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Introduction des données de base}{44}{subsection.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Simulation avec Eclipse 100}{44}{subsection.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Calage de l'historique de production (History matching)}{45}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Les deux grandes classes du history-matching}{46}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Qualité du calage de l'historique de production}{46}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}Calage du modèle du réservoir GRDC}{47}{subsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Prevision des performances du reservoir}{49}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Prévision des futurs comportements du réservoir}{49}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.1}Scénario 1}{49}{subsection.6.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.2}Scénario 2}{50}{subsection.6.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.3}Scénario 3}{52}{subsection.6.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.4}Scénario 4 (suite de scénario 3)}{56}{subsection.6.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\leavevmode {\color {ocre}Conclusion partielle}}{57}{chapter*.79}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\@mypartnumtocformat {II}{Implémentation et vérification des solveurs Open-source}}{58}{part.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Présentation et implémentation des solveurs BlackOil }{59}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}La structure d'une simulation OpenFOAM}{59}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.1}Le répertoire {\tt 0} }{60}{subsection.7.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.2}Le répertoire {\tt system} }{60}{subsection.7.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.3}Le répertoire {\tt constant} }{64}{subsection.7.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.4}Pourquoi choisir OpenFOAM comme use base du code?}{64}{subsection.7.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}La méthode des volumes finis dans OpenFOAM}{65}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.1}La discrétisation du domaine}{65}{subsection.7.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.2}La discrétisation des équations}{67}{subsection.7.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.3}Les conditions aux limites (BCs)}{71}{subsection.7.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.4}Utilitaires de maillage OpenFOAM}{72}{subsection.7.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.4.1}blockMesh}{72}{subsubsection.7.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.4.2}snappyHexMesh}{73}{subsubsection.7.2.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.5}La quality du maillage OpenFOAM}{74}{subsection.7.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.6}La résolution des systèmes des équations algébriques}{75}{subsection.7.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.6.1}Amélioration de la convergence}{75}{subsubsection.7.2.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.6.2} Contrôle de convergence dans les solveurs linéaires }{76}{subsubsection.7.2.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Implémentation des solveurs BlackOil}{76}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3.1} Une formulation compositionnelle généralisée pour les équations BlackOil}{76}{subsection.7.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3.2}La bibliothèque de support}{78}{subsection.7.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.2.1}Modélisation de la perméabilité relative}{78}{subsubsection.7.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.2.2}Modélisation de la pression capillaire}{79}{subsubsection.7.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.2.3}Modélisation des puits}{79}{subsubsection.7.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3.3}L'implémentation du solveur {\tt pSwImpesFoam}}{81}{subsection.7.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.3.1}Les équations résolues}{81}{subsubsection.7.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3.4}L'implémentation du solveur {\tt pSwCoupledFoam}}{85}{subsection.7.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.4.1}Technique de résolution en blocs}{86}{subsubsection.7.3.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.4.2}La sélection de la valeur {\tt timeStep}}{87}{subsubsection.7.3.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.4.3}Contrôle de convergence avec les CNVs}{87}{subsubsection.7.3.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.4.4}Avantages et inconvénients}{88}{subsubsection.7.3.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Benchmarks \& vérification des solveurs BlackOil}{89}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Benchmark 01: Buckley-Leverett}{89}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.1}Bechmark 01.A: Un écoulement basique de Buckley-Leverett}{90}{subsection.8.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.2}Benchmark 01.B: Rapport de mobilité inférieur à 1}{93}{subsection.8.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.3}Benchmark 01.C: Relations linéaires de permeabilité relative}{94}{subsection.8.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Benchmark 02: SPE10 Dataset 01}{95}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.2.1}Benchmark 02.A: Le cas original de l'SPE10}{96}{subsection.8.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.2.2}Benchmark 02.B: SPE10 avec effets de capillarité}{98}{subsection.8.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}La simulation du réservoir GRDC}{98}{section.8.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\leavevmode {\color {ocre}Conclusion partielle}}{101}{chapter*.121}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\setlength \fboxsep {0pt}\noindent \colorbox {ocre!40}{\relax \unhcopy \strutbox \parbox [c][.7cm]{\linewidth }{\relax \fontsize {17.28}{22}\selectfont \sffamilyFB \centering Conclusion \& Recommandations\hskip 1em\relax \unhbox \voidb@x \hbox {}}}}{102}{section*.122}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\leavevmode {\color {ocre}Conclusion générale}}{103}{chapter*.123}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\leavevmode {\color {ocre}Recommandations}}{104}{chapter*.124}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\setlength \fboxsep {0pt}\noindent \colorbox {ocre!40}{\relax \unhcopy \strutbox \parbox [c][.7cm]{\linewidth }{\relax \fontsize {17.28}{22}\selectfont \sffamilyFB \centering Bibliographie\hskip 1em\relax \unhbox \voidb@x \hbox {}}}}{105}{section*.125}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\leavevmode {\color {ocre}Bibliographie}}{106}{chapter*.126}
\contentsfinish 
