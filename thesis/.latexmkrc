# latexmk config file to use lualatex in vim
$pdflatex = 'lualatex -shell-escape -file-line-error %O %S';
$pdf_mode = 1;
# Custom dependency and function for nomencl package 
#add_cus_dep( 'nlo', 'nls', 0, 'makenlo2nls' );
#sub makenlo2nls {
#system( "makeindex -s nomencl.ist -o \"$_[0].nls\" \"$_[0].nlo\"" );
#system( "" );
#}

$makeindex = "xindy -M texindy -C utf8 -I latex -L english %S";

#$makeindex = 'makeindex main.nlo -s nomencl.ist -o main.nls';
@cus_dep_list = (@cus_dep_list, "glo gls 0 makenomenclature");
sub makenomenclature {
   system("makeindex $_[0].glo -s nomencl.ist -o $_[0].gls"); }
@generated_exts = (@generated_exts, 'glo');
