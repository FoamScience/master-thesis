\newpage
\section{Conventions du style}\label{conventions}

%Pour assurer un bon niveau de cohérence de ce document \LaTeX \relax\ , un style unifié 
%est utilisé tout au long de la thèse. Cette section le décrit brièvement:
%
\subsection*{Utilisation des polices}

Le texte de cette thèse est écrit avec une police Fira Go romaine (verticale). Le texte 
"mis en évidence" est écrit avec des lettres {\it italic} avec la même police ou entre deux 
symboles (""). Les autres polices sont utilisées à des fins diverses:

\begin{itemize}
    \item {\tt Ubuntu Mono} pour les noms des fichiers et de dossiers, les extraits de code 
    et les noms des solveurs (e.g. {\tt pSwImpesFoam}).

    \item Les équations et les symboles mathématiques sont composés en utilisant l'ensemble
    des polices par défaut de \LaTeX pour les symboles mathématiques (CM*). Une liste des 
    symboles et des abréviations utilisés est présentée juste après la liste des figures et 
    des tableaux. L'utilisation des symboles mathématiques suit généralement la 
    norme "ISO 80000-2"
    \footnote{https://en.wikipedia.org/wiki/ISO\_80000-2} avec quelques ajustements:
        \begin{itemize}
            \item Les vecteurs sont composés avec une flèche au lieu d'un style gras.
            %\item Le symbole Nabla ({$\nabla$}) est utilisé pour les opérateurs de divergence 
            %    et du gradient sans la flèche en cas du gradient, car l'opérateur désiré 
            %    est évident du contexte.
            \item De plus, le symbole Delta ($\Delta$) est utilisé indifféremment pour les 
                opérateurs Laplacien et Variation du variable. 
        \end{itemize}
    \item {\sf Exo Bold} est utilisé pour les titres des sections.
\end{itemize}

%\subsection*{Figures \& Couleurs}
%
%Dans les courbes résultatantes des solveurs, les couleurs ont une signification 
%(cela peut ne pas s'appliquer aux figure illustratives):
%
%\begin{itemize}
%    \item La couleur {\color{ocre} bleue foncée} est généralement utilisée pour les courbes 
%        de référence (résultats théoriques ou résultats d'un logiciel bien connu).
%    \item La couleur {\color{pRed} rouge} est utilisée pour {\tt pSwImpesFoam}. Si deux courbes 
%        d'un même solveur se chevauchent dans le même tracé, une couleur 
%        {\color{pOrange} secondaire} est utilisée.
%    \item La couleur {\color {pGreen} verte} est utilisée pour les résultats du 
%        {\tt pSwCoupledFoam}.
%\end{itemize}
%
%Physical properties with discontinuous non-diffusive behavior (e.g. phase saturation) are presented using the Viridis colormap (Default in Python's Matplotlib, minimal value at the left):\\
%Les propriétés physiques ayant un comportement non diffusif discontinu 
%(saturations des phases, par exemple) sont présentées à l'aide de la carte
%de couleurs Viridis (Le choix du bibliothèque Python Matplotlib, valeur minimale à
%gauche): \\
%\begin{tikzpicture}
%\pgfplotsset{ticks=none}
%\begin{axis}[
%  hide axis,
%  scale only axis,
%  height=0pt,
%  width=0pt,
%  colormap name=viridis,
%  colorbar horizontal,
%  colorbar style={
%    width=10cm,
%  }]
%\addplot [draw=none] coordinates {(0,0)};
%\end{axis}
%\end{tikzpicture}\\
%De l'autre côté, la pression est présentée à l’aide de la carte de couleurs allant 
%du froid au chaud (Le choix Paraview, valeur minimale à gauche) \\
%\begin{tikzpicture}
%\pgfplotsset{ticks=none}
%\begin{axis}[
%  hide axis,
%  scale only axis,
%  height=0pt,
%  width=0pt,
%  colormap={cooltowarm}{rgb255=(59,76,192) rgb255=(220.575765,220.575765,220.575765) rgb255=(180,4,38)},
%  colorbar horizontal,
%  colorbar style={
%    width=10cm,
%  }]
%\addplot [draw=none] coordinates {(0,0)};
%\end{axis}
%\end{tikzpicture}\\

\subsection*{Environnements techniques}

Les commandes entrées dans le shell (Bash) sont représentées comme dans \cref{sh:conv}. 
Les lignes des commandes sont numérotées et doivent être exécutées dans un répertoire: 
{\tt /some/caseDirectory}.

\begin{tcbshellcode}{/some/caseDirectory}{linenos=true,}{}
{% Results of command if any
Sample Solver Log}%
	{sh:conv}
pSwCoupledFoam | tee pSwCoupledFoam.log
\end{tcbshellcode}

Des portions de fichiers OpenFOAM sont souvent présentés comme indiqué dans \cref{ofconv}:

\begin{tcbfoamcode}{cavity/0/p}{ofconv}{}
movingWall
{
    type            zeroGradient;
}
\end{tcbfoamcode} \index{boundaryField!movingWall}

%Les notes sur des questions (profondément) techniques ne sont pas souvent abordées 
%dans le texte, mais plutôt mentionnées dans une boîte à remarques:
%
%\begin{tcbremark}[colbacktitle=green,remember as=one]{My title}
%Ceci est une remarque, ou quelques citations utiles.
%\end{tcbremark}
%    \begin{tikzpicture}[overlay,remember picture,line
%        width=2pt,draw=green!75!black]
%        \draw[->] (one.west) to[bend left] node[before]{} +(0,+5cm);
%    \end{tikzpicture}

%\begin{tcbwarning}[colbacktitle=red!70!yellow]
%    This is a really Important advice.
%\end{tcbwarning}
