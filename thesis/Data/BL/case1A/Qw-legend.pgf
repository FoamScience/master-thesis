%% Creator: Matplotlib, PGF backend
%%
%% To include the figure in your LaTeX document, write
%%   \input{<filename>.pgf}
%%
%% Make sure the required packages are loaded in your preamble
%%   \usepackage{pgf}
%%
%% Figures using additional raster images can only be included by \input if
%% they are in the same directory as the main LaTeX file. For loading figures
%% from other directories you can use the `import` package
%%   \usepackage{import}
%% and then include the figures with
%%   \import{<path to file>}{<filename>.pgf}
%%
%% Matplotlib used the following preamble
%%   \usepackage{fontspec}
%%
\begingroup%
\makeatletter%
\begin{pgfpicture}%
\pgfpathrectangle{\pgfpointorigin}{\pgfqpoint{5.000000in}{0.500000in}}%
\pgfusepath{use as bounding box, clip}%
\begin{pgfscope}%
\pgfsetbuttcap%
\pgfsetmiterjoin%
\definecolor{currentfill}{rgb}{1.000000,1.000000,1.000000}%
\pgfsetfillcolor{currentfill}%
\pgfsetlinewidth{0.000000pt}%
\definecolor{currentstroke}{rgb}{1.000000,1.000000,1.000000}%
\pgfsetstrokecolor{currentstroke}%
\pgfsetdash{}{0pt}%
\pgfpathmoveto{\pgfqpoint{0.000000in}{0.000000in}}%
\pgfpathlineto{\pgfqpoint{5.000000in}{0.000000in}}%
\pgfpathlineto{\pgfqpoint{5.000000in}{0.500000in}}%
\pgfpathlineto{\pgfqpoint{0.000000in}{0.500000in}}%
\pgfpathclose%
\pgfusepath{fill}%
\end{pgfscope}%
\begin{pgfscope}%
\pgfsetbuttcap%
\pgfsetmiterjoin%
\definecolor{currentfill}{rgb}{1.000000,1.000000,1.000000}%
\pgfsetfillcolor{currentfill}%
\pgfsetfillopacity{0.800000}%
\pgfsetlinewidth{1.003750pt}%
\definecolor{currentstroke}{rgb}{1.000000,1.000000,1.000000}%
\pgfsetstrokecolor{currentstroke}%
\pgfsetstrokeopacity{0.800000}%
\pgfsetdash{}{0pt}%
\pgfpathmoveto{\pgfqpoint{0.974931in}{0.032570in}}%
\pgfpathlineto{\pgfqpoint{4.025069in}{0.032570in}}%
\pgfpathquadraticcurveto{\pgfqpoint{4.052847in}{0.032570in}}{\pgfqpoint{4.052847in}{0.060347in}}%
\pgfpathlineto{\pgfqpoint{4.052847in}{0.439653in}}%
\pgfpathquadraticcurveto{\pgfqpoint{4.052847in}{0.467430in}}{\pgfqpoint{4.025069in}{0.467430in}}%
\pgfpathlineto{\pgfqpoint{0.974931in}{0.467430in}}%
\pgfpathquadraticcurveto{\pgfqpoint{0.947153in}{0.467430in}}{\pgfqpoint{0.947153in}{0.439653in}}%
\pgfpathlineto{\pgfqpoint{0.947153in}{0.060347in}}%
\pgfpathquadraticcurveto{\pgfqpoint{0.947153in}{0.032570in}}{\pgfqpoint{0.974931in}{0.032570in}}%
\pgfpathclose%
\pgfusepath{stroke,fill}%
\end{pgfscope}%
\begin{pgfscope}%
\pgfsetrectcap%
\pgfsetroundjoin%
\pgfsetlinewidth{1.505625pt}%
\definecolor{currentstroke}{rgb}{0.000000,0.008000,0.504000}%
\pgfsetstrokecolor{currentstroke}%
\pgfsetdash{}{0pt}%
\pgfpathmoveto{\pgfqpoint{1.002708in}{0.361042in}}%
\pgfpathlineto{\pgfqpoint{1.280486in}{0.361042in}}%
\pgfusepath{stroke}%
\end{pgfscope}%
\begin{pgfscope}%
\definecolor{textcolor}{rgb}{0.000000,0.000000,0.000000}%
\pgfsetstrokecolor{textcolor}%
\pgfsetfillcolor{textcolor}%
\pgftext[x=1.391597in,y=0.312430in,left,base]{\color{textcolor}\fontsize{10.000000}{12.000000}\selectfont Theory}%
\end{pgfscope}%
\begin{pgfscope}%
\pgfsetrectcap%
\pgfsetroundjoin%
\pgfsetlinewidth{1.505625pt}%
\definecolor{currentstroke}{rgb}{0.492000,0.768000,0.268000}%
\pgfsetstrokecolor{currentstroke}%
\pgfsetdash{}{0pt}%
\pgfpathmoveto{\pgfqpoint{1.002708in}{0.163681in}}%
\pgfpathlineto{\pgfqpoint{1.280486in}{0.163681in}}%
\pgfusepath{stroke}%
\end{pgfscope}%
\begin{pgfscope}%
\definecolor{textcolor}{rgb}{0.000000,0.000000,0.000000}%
\pgfsetstrokecolor{textcolor}%
\pgfsetfillcolor{textcolor}%
\pgftext[x=1.391597in,y=0.115069in,left,base]{\color{textcolor}\fontsize{10.000000}{12.000000}\selectfont pSwCoupledFoam}%
\end{pgfscope}%
\begin{pgfscope}%
\pgfsetrectcap%
\pgfsetroundjoin%
\pgfsetlinewidth{1.505625pt}%
\definecolor{currentstroke}{rgb}{0.952000,0.256000,0.212000}%
\pgfsetstrokecolor{currentstroke}%
\pgfsetdash{}{0pt}%
\pgfpathmoveto{\pgfqpoint{2.703403in}{0.361042in}}%
\pgfpathlineto{\pgfqpoint{2.981181in}{0.361042in}}%
\pgfusepath{stroke}%
\end{pgfscope}%
\begin{pgfscope}%
\definecolor{textcolor}{rgb}{0.000000,0.000000,0.000000}%
\pgfsetstrokecolor{textcolor}%
\pgfsetfillcolor{textcolor}%
\pgftext[x=3.092292in,y=0.312430in,left,base]{\color{textcolor}\fontsize{10.000000}{12.000000}\selectfont pSwImpesFoam}%
\end{pgfscope}%
\end{pgfpicture}%
\makeatother%
\endgroup%
