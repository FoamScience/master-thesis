%\section{Introduction}

\section{Introduction Générale} \label{sec:intro}

La capacité estimée des réserves d'hydrocarbures existant laisse présager un 
épuisement total dans les 80 prochaines années.  Comme il n’existe actuellement 
aucune solution de rechange quantitative et qualitative à la portée de la main, 
il est de toute évidence nécessaire d’améliorer la récupération des gisements
d’hydrocarbures existants et futurs. Des modèles computationnels de réservoir, basés sur
des données sismiques et géologiques, sont développés pour créer des prévisions à court et
à long terme. Les ingénieurs de production suivent ces plans pour maximiser les
performances de production du réservoir, confirmer la politique de l'entreprise, atteindre
les objectifs économiques et enfin améliorer la production totale sur le terrain.

Au cours des dernières décennies, la demande en modèles de réservoir modernes, créés par
de puissants simulateurs, a augmenté rapidement. De nouvelles fonctionnalités et
améliorations facilitent la collecte, le traitement et la visualisation des données des
réservoirs contenant les hydrocarbures et, enfin, le processus de prise de décision. 

Les projets de récupération des hydrocarbures, qui nécessitent des investissements
importants, sont risqués car ils dépendent de stratégies de développement et de production
précaires. Pour minimiser l'exposition aux risques, la simulation de réservoir moderne est utilisée
pour obtenir les résultats d'exploitation les plus élevés possibles et pour étudier les
stratégies de production pertinentes. 

L’acceptation croissante de la simulation professionnelle des réservoirs dans l’industrie
pétrolière au cours des dernières décennies peut également être attribuée aux progrès
réalisés dans les installations informatiques. Diverses sociétés ont introduit un certain
nombre de logiciels de simulation de haute qualité pour résoudre 
des écoulements multiphasiques dans des réservoirs complexes. Les
logiciels de réservoir communs peuvent être classés en deux catégories: commerciaux et
open source.

Le principal inconvénient des logiciels commerciaux de simulation de réservoirs est le
coût des licences, qui peut s’élever à 35 000 euros par an et par ordinateur.  les coûts
de licence élevés, la structure complexe du simulateur de réservoir moderne et la
restriction d'accès à son code source constituent des obstacles lorsque le logiciel de
simulation de réservoir commercial doit être utilisé à des fins académiques.

Mais, pour simuler  des systèmes présentant un intérêt pratique avec un simulateur open
source , il est essentiel de disposer de schémas robustes et efficaces avec la méthode des
volumes finis (FVM) et de stratégies de solution pour les PDEs du problème. C’est le motif
le plus important pour lequel nous avons choisi OpenFOAM (Article original: \autocite{foamorg}) 
comme une base pour nos solveurs de simulation de réservoir.

La prévision de l’évolution des champs de pression et de saturation en phase fluide
implique la résolution des équations aux dérivées partielles (PDE) modélisant le transport
multiphasique en milieu poreux. La résolution de ces PDEs pour des modèles de réservoir à
haute résolution décrivant une géologie réaliste est un défi.

Les milieux géologiques poreux sont très hétérogènes. Les variations de perméabilité
couvrant plusieurs ordres de grandeur construit un large domaine de vitesses d’écoulement
et d’échelles de temps. Pour les méthodes de résolution basées sur une discrétisation
temporelle explicite, telle que la méthode IMPES (Implicit Pressure Explicit Saturation),
cette plage étendue d’échelles de temps impose des restrictions sévères à la taille du pas
de temps autorisée \autocite{coats2000note, coats2003impes}. La méthode implicite adaptative
(AIM) permet de réaliser des pas de temps stables plus importants en traitant les
saturations de manière implicite dans les volumes de contrôle soumis à des changements de
saturation importants et/ou à un débit de fluide élevé, ce qui correspond aux grands
nombres de Courant-Friedrichs-Lewy (CFL) \autocite{russell1989stability}. Cependant, pour les problèmes
fortement couplés non linéaires, la discrétisation temporelle du choix est la méthode
totalement implicite (backward-Euler) dans laquelle tous les degrés de liberté sont
évalués implicitement pour atteindre une stabilité inconditionnelle.

%La simulation numérique de l'écoulement de fluide dans les milieux poreux est un outil essentiel 
%pour améliorer notre compréhension des réservoirs souterrains et des processus de récupération 
%du pétrole. La prévision de l'évolution des champs de pression et de saturation en phase fluide 
%implique la résolution des équations aux dérivées partielles (PDE) modélisant le 
%transport multiphasique en milieu poreux. La résolution de ces PDEs pour des modèles de réservoir 
%à haute résolution décrivant une géologie réaliste est un défi. Premièrement, l'évolution du 
%problème des transports est hautement non linéaire. Plus précisément, dans les lois de conservation 
%de masse, la non-monotonie du flux analytique entraîne une dynamique de transport complexe 
%caractérisée par un écoulement à contre-courant et une propagation des chocs. Par conséquent, 
%les coefficients non linéaires dépendant de la saturation dans les PDEs - à savoir la perméabilité 
%relative et la pression capillaire - subissent des changements brusques lorsque les fronts de 
%saturation se propagent dans le milieu poreux. De plus, les processus d'écoulement et de 
%transport hautement non linéaire, entraînés par l'interaction complexe des forces visqueuses, 
%des forces capillaires et des forces de gravité, sont étroitement liés \autocite{peacemanfund}.
%
%Ainsi, afin de pouvoir utiliser des prévisions basées sur des simulations pour gérer des systèmes 
%présentant un intérêt pratique, il est essentiel de disposer de schémas robustes et
%efficaces avec la méthode des volumes finis (FVM) et de stratégies de solution pour les
%PDEs du problem. C’est le motif le plus important pour lequel nous avons choisi OpenFOAM\textregistered 
%\autocite{foamorg} comme une base pour nos solveurs de simulation de réservoir.
%
\nomenclature[Ap]{IMPES}{IMplicit Pressure Explicit Saturation}
\nomenclature[Ap]{AIM}{Adaptive Implicit Method}
%Les milieux géologiques poreux sont très hétérogènes. Les variations de perméabilité couvrant 
%plusieurs ordres de grandeur construit un large domaine de vitesses d'écoulement et d'échelles 
%de temps. Pour les méthodes de résolution basées sur une discrétisation temporelle explicite, telle que 
%la méthode IMPES (Implicit Pressure Explicit Saturation), cette plage étendue d'échelles de temps impose 
%des restrictions sévères à la taille du pas de temps autorisée \autocite{coats2000note, coats2003impes}. 
%La méthode implicite adaptative (AIM) permet de réaliser des pas de temps stables plus importants en 
%traitant les saturations de manière implicite dans les volumes de contrôle soumis à des changements 
%de saturation importants et/ou à un débit de fluide élevé, ce qui correspond aux grands nombres de 
%Courant-Friedrichs-Lewy (CFL) \autocite{russell1989stability}. Cependant, pour les problèmes fortement 
%couplés non linéaires, la discrétisation temporelle du choix est la méthode totalement implicite 
%(backward-Euler) dans laquelle tous les degrés de liberté sont évalués implicitement pour 
%atteindre une stabilité inconditionnelle.



\section{Objectifs de ce travail}

Les objectifs principaux de cette thèse peuvent être résumés comme suit: 
\begin{enumerate} 
    \item 
        Etudier le réservoir GRDC-HMD pour construire un modèle de simulation fiable,  
        afin de  prévoir les performances futures du réservoir selon les différentes stratégies 
        de developpement proposées, visant l’amélioration du  taux de récupération.
    \item
        Développer un framework numérique flexible (en utilisant principalement la méthode des volumes 
        finis (FVM)) pour la simulation biphasique des réservoirs (y compris le GRDC-HMD) en se basant sur des
        logiciels Open-Source.
\end{enumerate}


\section{Méthodologie de l'étude}

L'étude du réservoir GRDC requit la génération du modèle dynamique, qui nécessite des
études antérieures  permettant de déterminer les différents paramètres pétro physiques
(modèle statique), ainsi que les propriétés des fluides du résrvoir (étude PVT,SCAL). Le
bilan matière sert à estimer les réserves en place et à déterminer les mécanismes de
drainage.  Ensuite, le modèle initial est ajusté par ce qui est appellé le calage de
l‘historique de production, pour que le modèle de simulation prévoie  correctement  les
performances futures du réservoir avec une marge d’erreur acceptable.  Enfin, le modèle
calé est utilisé pour l’étude prévisionnelle en propsant plusieurs scénarios visant à
maximiser la production et  l’amélioration des performances du réservoir.

Une autre partie de cette étude est une tentative à programmer une bibliothèques de modèles
et des solveurs biphasiques basées sur le modèle Black Oil \autocite{chen2000formulations, el2009realtime}, 
pour simuler le réservoir GRDC-HMD, en
utilisant la plate-forme d'un système open-source de "Computational Fluid Dynamics" (CFD), 
nommé OpenFOAM\textregistered. OpenFOAM \textregistered est développé principalement par OpenCFD Ltd. 
depuis 2004, distribué par OpenCFD Ltd. et par OpenFOAM\textregistered Foundation. Il fournit une 
plate-forme pour la résolution numérique des équations aux dérivées partielles impliquées dans CFD et 
ses matrices en blocs (disponibles dans Foam-Extend) fournissent un moyen facile à utiliser pour 
les simulations implicites.

%L'une des fonctionnalités les plus intéressantes de la plate-forme OpenFOAM est sa gestion 
%dynamique du maillage. Bien que ce ne soit pas utilisé dans ce travail; c'est une opportunité pour 
%optimiser le suivi des contacts fluides. De plus, l'accessibilité du code source d'OpenFOAM facilite 
%l'ajout de fonctionnalités nécessaires à la construction du simulateur de réservoir 
%(puits, perméabilité relative, pression capillaire, etc.).

La version {\tt FOAM-extend-4.0} distribuée par OpenFOAM Extend-Project est utilisée dans
ce travail. Le
projet FoamExtend a pour objectif d'ouvrir la boîte à outils OpenFOAM aux développements de la 
communauté, dans l'esprit du modèle de développement open source.

La deuxième partie de cette thèse décrit principalement deux solveurs de BlackOil biphasiques et isothermes: {\tt pSwImpesFoam} 
et {\tt pSwCoupledFoam}, avec leurs versions anisotropes; chacun a ses propres propriétés et étendue 
d'application. Toute une bibliothèque de modèles (infrastructure de base) a été développée pour 
supporter ces solveurs:

\begin{enumerate} 
    \item Des facilités de modélisation des puits: implémente le modèle vertical de base
        de "Peaceman" \autocite {chen2009well} avec un contrôle par BHP imposée. 
        \nomenclature[Ap]{BHP}{Bottom Hole Pressure (Pression dynamique du fond)} 
    \item Des modèles pour la perméabilité relative, par exemple, le modèle Brooks-Corey \autocite{brookscorey}. 
    \item Des modèles pour la pression capillaire, par exemple, le modèle Brooks-Corey \autocite{brookscorey}.

    %\item La détection de la génération du gaz libre (point de bulle): implémente un modèle de pression
    %    à point de bulle fixée et un modèle "flash BlackOil" \autocite{wang2007bubble}. 
    \item Des utilités et "plugins" pour le traitement auxiliaire des résultats (par exemple,
        la {\tt functionObject wellFlowRate} pour rapporter les débits des puits.
\end{enumerate}

La performance des solveurs est testée par des cas SPE applicables sélectionnés. Tout d'abord, les 
composants individuels de la bibliothèque sont testés avec des cas de test préliminaires 
(situations numériques extrêmes). Ensuite, la performance globale de chaque solveur est testée en 
simulant un modèle SPE10 bidimensionnel complexe (Data set 01); avant de simuler le cas réel du 
réservoir GRDC. %et de comparer les résultats avec MUFITS\textregistered et ECLIPSE\textregistered.
%
%Nous sommes parfaitement conscients du fait que "l'extensibilité" de la bibliothèque développée 
%ne se compare pas à l'extensibilité d'origine des classes OpenFOAM. Ceci est principalement dû à 
%l'utilisation réduite des "templates" facilitant la manipulation du code dans le temps imparti à la thèse, 
%et la situation changera par la suite (On espère qu'elle va changer).

Bien que nous ayons fait de notre mieux pour garder les fonctionnalités parallèles d'OpenFOAM 
intactes, les simulations parallèles n'ont pas été testées de manière approfondie. Les
simulations exécutées par {\tt pSwCoupledFoam \& pSwImpesFoam} ont été effectuées en série 
avec un ordinateur portable HP Pavilion g6 (Intel(R) Core(TM) i5-3210M CPU @ 2.50GHz,4 Go de RAM).

Pour évaluer la validité et la précision des solveurs développés, des résultats théoriques sont utilisés 
si possible (l'écoulement de Buckley-Leverett principalement). Pour des problèmes plus complexes, 
les résultats d'une simulation identique utilisant le logiciel MUFITS sont comparés 
aux résultats OpenFOAM.

\section{Investigation \& critique des travaux antérieurs} \label{sec:previouswork}

Il existe des outils open source pour la simulation de réservoirs; à savoir MATLAB Reservoir Simulation 
Toolbox (MRST) \nomenclature[Ap]{MRST}{MATLAB Reservoir Simulation Toolbox} \autocite{mrst} et 
l'initiative "Open Porous Media" (OPM). Mais, ils sont caractérisés par
leur structure de code complexe, ce qui rend difficile la création de nouveaux
solveurs en utilisant la bibliothèque existante. Au moins, cela prendrait trop du temps.

\subsection{Manipulation officielle des régions poreuses dans OpenFOAM}

Les versions officielles d'OpenFOAM prennent en charge, dans certaine mesure, les milieux poreux 
\autocite{hafsteinsson2009porous}: la porosité est modélisée en atténuant la dérivée temporelle 
et en ajoutant un terme source aux équations de Navier-Stokes:

\begin{itemize} 
    \item La porosité est introduite dans la dérivée temporelle. 
    \item Le terme source est généralement modélisé avec l’équation de Darcy-Forchheimer ou 
        sous la forme d’une simple loi de puissance de la vitesse du fluide. 
\end{itemize}
Cela fonctionne très bien pour les simulations industrielles (conception d'équipement du
traitement chimique par des régions poreuses, ... etc.) mais, bien sûr, cela ne convient pas aux 
simulations à l'échelle du réservoir.

\subsection{Solveurs pour la simulation monophasique de réservoir }

Stephan Reitler a utilisé un solveur OpenFOAM {\tt PRSFoam}(développé par HOL GmbH) dans
\cite{PRSFoam} pour simuler l'écoulement d'huile dans les réservoirs. Le solveur 
est apparemment à source fermée et très restrictif en termes de caractéristiques
d'écoulement:

\begin{enumerate}
    \item Écoulement monophasique incompressible avec compression de la roche. 
    \item Il n'y a pas de gaz libéré à l'état initial et pendant la production.
        %supposition, mais rien n'a été fait pour assurer sa validité)
\end{enumerate}

Donc, c'est (virtuellement, puisque le code source n'est pas disponible) une version 
améliorée du solveur le plus élémentaire d'OpenFOAM {\tt scalarTransportFoam}; à savoir 
résoudre l'équation de diffusivité au lieu de l'équation générale de transport, en prenant 
en charge le contrôle des puits avec BHP constante.

Les cas de test, comme prévu pour les solveurs des problèmes physiques simples, montre
une bonne cohérence avec les résultats ECLIPSE (la différence de production maximale est inférieure 
à 7\% par 20 ans dans tous les cas de test, avec d'excellents cas ou la différence est inférieure à 
1\%). Mais nous trouvons qu'il est injuste de comparer le temps d'exécution pour {\tt PRSFoam} et 
ECLIPSE simplement parce qu'un temps d'exécution plus court est attendu pour {\tt PRSFoam}
par design (il traite des problèmes plus simples numériquement).

L'article \autocite{diasreservoir} est une autre tentative pour simuler l'écoulement monophasique. Un modèle de 
diffusivité hydraulique a été implémenté dans OpenFOAM\textregistered et ANSYS Fluent\textregistered; 
Il convient de noter que leur solveur OpenFOAM {\tt hydraulicDiffusivityFoam} résout les écoulements 
compressibles avec une viscosité dépendante de la pression dans un milieu compressible, ce qui 
présente une légère amélioration par rapport à {\tt PRSFoam}, mais le code n’a pas été publié.


\subsection{Une boîte à outils pour l'écoulement biphasique dans un milieux poreux}

L'article \autocite{Horgue2015} décrit le développement d'une boîte à outils un peu plus sophistiquée pour simuler 
l'écoulement à travers un milieu poreux. Malheureusement, nous n'avons pas vu leur code avant que 
{\tt pSwImpesFoam} soit déjà en bon état; mais leur code nous a aidés à trouver des bugs et à 
ajouter des fonctionnalités importantes au solveur; En fait, le mécanisme du
"TimeStepping" de {\tt pSwImpesFoam} est tout à eux.

La boîte à outils a une structure de code satisfaisante qui permet de simuler un écoulement 
incompressible en deux phases dans un milieu poreux anisotrope avec prise en charge de la condition 
aux limites de Darcy pour le champ de pression. La bibliothèque prend en charge certains modèles 
de perméabilité relative et de pression capillaire, mais ne se concentre pas sur la
modélisation des puits.

\section{Plan de la thèse}  \label{sec:outline}

Ce travail est divisé en deux parties:
\begin{enumerate}
    \item L'analyse et la prédiction des performances du réservoir GRDC: On commence avec
        une présentation du champ Rhoudre Chegga (Chapitre 2), puis une méthodologie pour
        obtenir les courbes de permeabilité relative et les paramètres PVT est présenté
        dans le chapitre 3. Ensuite, les chapitre 4 \& 5 présentent les résultats d'estimation des
        réserves et de la simulation du réservoir (aprés History Matching), respectivement. 
        Enfin, la prévision des performances du réservoir est présentée dans le chapitre
        6.
    \item L'implémentation et la vérification des solveurs Open-Source: 
        D'abords, le chapitre 7 présente le software OpenFOAM et les solveurs développés
        (Equations résolus, aspects techniques), ensuite, le chapitre 8 décrit des cas de
        Benchmark (Buckley-Leverett, SPE10) et la tentative à simuler le réservoir GRDC.
\end{enumerate}
