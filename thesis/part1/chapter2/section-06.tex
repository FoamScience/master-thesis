%\section{Vérification \& problèmes Benchmark}

Trois simulations ont été utilisées pour la vérification de la robustesse et 
l'efficacité des solveurs présentés dans le chapitre précédent. Ces tests ont 
été sélectionnés car ils représentent un large éventail de problèmes physiques 
d’intérêt. Une attention particulière est accordée à la gravité, 
à la pression capillaire et aux milieux hautement hétérogènes. 

Le Benchmark 01 est la solution de l'équation de Buckley-Leverett \autocite{buckley1942mechanism}. 
Ce cas est utile pour la vérification puisqu’une solution analytique existe pour
l’équation.

Le Benchmark 02 est extrait du projet de solution comparative SPE 2001 \autocite{christie2001tenth}, 
organisé par la Society of Petroleum Engineers. Le but du projet était de fournir des 
données de référence pour comparer la performance de différents simulateurs et algorithmes. 
Le cas sélectionné est le SPE10-01, une configuration 2D avec des effets de gravité et une 
distribution de perméabilité très hétérogène. Les résultats sont comparés avec les travaux de 
\autocite{franc2016benchmark} et d'une simulation MUFITS identique.
\nomenclature[A]{MUFITS}{Un simulateur de réservoir non commercial pour l'analyse 
d'écoulements multiphasiques non isothermes en milieu poreux}

Le troisième cas est une simulation du réservoir GRDC-HMD en considérant un écoulement
bi-phasique dans un milieu anisotrope.

\section{Benchmark 01: Buckley-Leverett} \label{sec:benchmark01}

In this case, the water (wetting) phase is injected from the left into an uniform domain (100 m) saturated with oil (nonwetting) phase. At the right side of the domain, a fixed pressure condition is imposed. The flow is 1D and can be described using the Buckley-Leverett solution
L'eau (phase mouillante) est injectée de gauche dans un millieu poreux (100 m, porosité de
0.2) saturée en huile (phase non mouillante). Sur le côté droit du domaine, une condition 
de pression fixe est imposée. Le flux est unidimensionnel et peut être décrit à l'aide de la solution Buckley-Leverett
\autocite{buckley1942mechanism}.

Deux scénarios ont été testées avec différents paramètres en utilisant le modèle 
Brooks-Corey pour la perméabilité relative, et une troisième en utilisant le modèle 
{\tt tabulated}. Les paramètres pour tous les cas sont listés dans
\cref{tab:relperm01}. Les effets capillaires et gravitationnels sont négligés dans tous ces
trois cas.
\begin{table}[tb]
    \input{Tables/part1/chapter2/tab0105.tex}
    \caption{Les paramètres de permeabilité relative pour le benchmark 01}
    \label{tab:relperm01}
\end{table}
\nomenclature[Sp]{$S_{wc}$}{La saturation critique de l'eau}
\nomenclature[Sp]{$S_{or}$}{La saturation résiduelle d'huile}

Le flux 1D est théoriquement modélisé avec l'équation de déplacement frontal
\cref{eq:buckleyleverett}
\begin{ceqn}\begin{align}
    \frac{\partial S_w}{\partial t} + \nabla.\left(\frac{F}{\phi}f_w(S_w)\right) = 0
    \label{eq:buckleyleverett}
\end{align}\end{ceqn}
L'état initial suppose un domaine principalement saturé d'huile
$S_w^{t=0} = S_{wc}$. 
Les conditions aux limites sont représentées par un puits d'injection
($6.826\ 10^{-6} m^3/s$ de débit-imposé, à gauche du domaine) et un puits de production 
(à droite du domaine, contrôlé avec une BHP constante).
De plus, dans OpenFOAM, le "patch" de limite gauche adhère à une BC {\tt fixedValue} avec une 
valeur uniforme de $1-S_{or}=0.8$ et à un puits de production (à droite du domaine, contrôlé avec une constante BHP). ).

$F$ représente le flux total d’huile et d’eau: le flux choisi pour surveiller les erreurs de 
continuité dans nos solveurs car l’équation de continuité pour un flux incompressible indique que 
$\nabla.F= 0 $ qui force la valeur de $F$ à être égal au débit d'injection.

Dans les cas suivants, {\tt pSwCoupledFoam} et {\tt pSwImpesFoam} signalent une erreur de 
continuité globale de 0.0 (erreur cumulée < 1 $10^{-17}$) et la valeur du flux est égale 
au débit d'injection $9.826\ 10^{-6}\ m^3s^{-1}$. La configuration numérique pour tous les
cas est décrite par \cref{tab:itersolvers01}.
\begin{table}[tb]
    \input{Tables/part1/chapter2/tab0106.tex}
    \caption{La configuration des solveurs linéaires pour le benchmark 01}
    \label{tab:itersolvers01}
\end{table}

\subsection{Bechmark 01.A: Un écoulement basique de Buckley-Leverett}
\label{sec:benchmark01A}

Les objectifs principaux de ce test sont de:
\begin{enumerate}
    \item 
        Vérifier l'implémentation des puits opérés par BHP .
    \item 
        Défier le mécanisme de sélection du pas du temps IMPES sur les maillages moins
        fins (100 cellules, où le pas de temps devrait être relativement important).
\end{enumerate}

\cref{im:wellsaturation01A} présente le profil de saturation en eau du puits de production 
en comparant {\tt pSwImpesFoam} et {\tt pSwCoupledFoam} avec un profil théorique
de MMbbls \textregistered Buckley-Leverett web-software
\footnote{https://mmbbls-app.glassworks.no/}.
Cela montre un bon accord (généralement) des deux solveurs avec les résultats théoriques.


Après la percée, {\tt pSwImpesFoam} commet une erreur maximale de 1.8 \% 
dans l'estimation de la quantité d'eau atteignant le puits.%, mais un comportement 
%oscillant dans le débit d'eau est observé, ce qui est dû à deux facteurs:
%\begin{itemize}
%   \item 
%        Le traitement explicite de la pression dans la source du puits pour l’équation 
%        de saturation est responsable de la présence de telles oscillations de débit 
%        (Voir \cref{im:wellwaterrate01A}, pas d’oscillations avec {\tt pSwCoupledFoam}).
%   \item 
%        L'amplitude de ces oscillations est réduite en raffinant le maillage, ce qui 
%        réduit le pas de temps (voir le graphe secondaire \cref{im:wellwaterrate01A}) en comparant 
%        un maillage {\color{pOrange} de 300 cellules} avec les 100 cellules d'origine).
%\end{itemize}

La discontinuité au front (graphe secondaire de droite dans \cref{im:wellsaturation01A}) est
mieux approximée avec {\tt pSwCoupledFoam} en utilisant le schéma temporel {\tt backward} 
avec le maillage moins fin. Alors que les deux solveurs génèrent un temps de Breakthrough raisonnable, 
{\tt pSwCoupledFoam} converge beaucoup plus rapidement vers la solution théorique.
Les débits de puits dans les deux solveurs sont calculés à l'aide 
d'un {\tt functionObject} (plugin) qui récupère les coefficients de contribution du puits directement à partir 
de la matrice à la fin de chaque pas de temps.

\cref{im:saturationoverdistance01A} indique la position du front 100, 400 et 800 jours après le début de l’injection: 
les deux solveurs offrent un suivi du front presque identiques. Après 3500 jours, 
le facteur de récupération d'huile attendu est de 65.82\%. Les données du solveur couplé 
conduisent à un facteur de 66.18 \% (l’erreur relative est de 0.5 \% sur 9.5 ans et est 
introduite par la capture imprécise du front). IMPES commet une erreur de 1.03 \% en prédisant 
un facteur de récupération de 66.55 \%. Considérant que le maillage n'est pas fin
($\Delta x = 1 \ m$), ces résultats présentent une très bonne approximation.
\begin{figure}[!ph]
    \centering
    \begin{subfigure}[t]{\textwidth}
    \centering
    \input{Data/BL/case1A/Qw-legend.pgf}
    \input{Data/BL/case1A/Saturation-profiles.pgf}
    \caption{L'évolution de la saturation d'eau au puits producteur.}
    \label{im:wellsaturation01A}
    \end{subfigure}\\
    \begin{subfigure}[t]{.5\textwidth}
    \centering
    \adjustbox{max width=\textwidth}{
    \input{Data/BL/case1A/Qw.pgf}
    }
    \caption{Profile du débit d'eau pour le puits producteur}
    \label{im:wellwaterrate01A}
    \end{subfigure}~
    \begin{subfigure}[t]{.5\textwidth}
    \centering
    \adjustbox{max width=\textwidth}{
    \input{Data/BL/case1A/Qo.pgf}
    }
    \caption{Profile du débit d'huile pour le puits producteur}
    \label{im:welloilrate01A}
    \end{subfigure}
    %\begin{subfigure}[t]{\textwidth}
    %\centering
    %\input{Data/BL/case1A/Saturation-over-distance.pgf}
    %\caption{Saturation over distance from injector.}
    %\label{im0208c}
    %\end{subfigure}
    \caption{Profile de la saturation en eau et les débits au puits producteur pour le Benchmark 01.A}
    \label{im:benchmark01wellresults}
\end{figure}
\begin{figure}[tb]
    \centering
    \input{Data/BL/case1A/Saturation-over-distance-legend.pgf}
    \input{Data/BL/case1A/Saturation-over-distance.pgf}
    \caption{La saturation en eau en fonction de la distance à l'injecteur pour le Benchmark
    01.A}
    \label{im:saturationoverdistance01A}
\end{figure}
\newpage

\subsection{Benchmark 01.B: Rapport de mobilité inférieur à 1}\label{sec:benchmark01B}

Les objectifs principaux de ce test sont de:
\begin{enumerate}
    \item 
        Tester les schémas du temps et trouver la configuration optimale.
    \item 
        Vérifier le comportement du solveur lorsque le ratio de mobilité est supérieur à un.
\end{enumerate}

\begin{figure}[bt]
    \centering
    \input{Data/BL/case1B/Saturation-profiles-legend.pgf}
    \input{Data/BL/case1B/Saturation-profiles.pgf}
    \caption{La saturation en eau au puits producteur pour le Benchmark 01.B}
    \label{im:wellsaturation01B}
\end{figure}

Ce benchmark simule un écoulement de Buckley-Leverett moins efficace (mobilité supérieure à un) 
sur un maillage plus fin, avec un changement maximal de la saturation en eau par cellule 
($\Delta S_{max} = 0.001$), ce qui génère de faibles pas de temps dans les deux
solveurs. solveurs. Pour obtenir un résultat optimal, chaque solveur est exécuté avec un schéma 
temporel différent en raison de la nature numérique différente des équations de saturation:
\begin{itemize}
    \item 
        Avec {\tt pSwImpesFoam}, il s'agit d'une matrice diagonale: 
        On choisit Crank-Nicolson avec $ \psi = 0.9 $.
    \item Avec {\tt pSwCoupledFoam}, il s'agit d'une matrice symétrique en block. 
        Le schéma Euler fournit des résultats acceptables.
\end{itemize}

En ce qui concerne le facteur de récupération après 2000 jours, les deux solveurs donnent
une valeur de 41.66\%, qui correspond à la valeur théorique de 41.23\% 
(l’erreur relative est de 1 \%).

\newpage
\subsection{Benchmark 01.C: Relations linéaires de permeabilité
relative}\label{sec:benchmark01C}

Les objectifs principaux de ce test sont de:
\begin{enumerate}
    %\item 
        %Compare the performance of {\tt pSwCoupledFoam} and {\tt pSwImpesFoam} under different circumstances of mesh size and linear tolerance.
    \item 
        Vérifier le comportement des solveurs dans le cas de courbes linéaires de 
        perméabilité relative.
\end{enumerate}

\begin{figure}[bt]
    \centering
    \input{Data/BL/case1C/Coupled-Impes-Mufits-DeltaT-legend.pgf}
    \input{Data/BL/case1C/Coupled-Impes-Mufits-DeltaT.pgf}
    \caption{Une comparaison des performances des solveurs développés pour le Benchmark 01.C 
    en faisant varier la taille du maillage}
    \label{im:delta01C}
\end{figure}

{\tt BrooksCorey} ne contient pas de code spécial pour traiter correctement le cas des courbes 
de perméabilité relative linéaire; Ce qui ne serait pas trivial dans de nombreux cas. Le 
modèle approprié est {\tt tabulated}: La transition entre la variation linéaire et la
région (des valeurs de saturation) ou la permeabilité relative est constante provoque une 
discontinuité dans la dérivée de la perméabilité, ce qui entraîne le dépassement de la
saturation des bornes autorisée. Pour résoudre ce problème, nous devons lisser les dérivées 
autour des extrémités (voir \cref{foam:krBLC}).

\begin{tcbfoamcode}{BL-Case1C/kr.dat}{foam:krBLC}{}
   //(Sw         (krw  kro  dkrwdS  dkrodS)
     (0.0        (0.0  0.5  0.0     0.0   ))
     (0.2        (0.0  0.5  0.0     0.0   ))
     (0.2000001  (0.0  0.5  1.5     -0.83 ))
     (0.7999999  (0.9  0.0  1.5     -0.83 ))
     (0.8        (0.9  0.0  0.0     0.0   ))
     (1.0        (0.9  0.0  0.0     0.0   ))
\end{tcbfoamcode}\index{tabulated}%

La sélection du $\Delta t$ en case du IMPES et du solveur couplé est comparé à une solution totalement 
implicite en utilisant MUFITS \textregistered\relax. Contrairement à l'approche totalement implicite, 
le solveur couplé doit encore augmenter son pas de temps progressivement (au plus de 20 \% à chaque fois dans ce cas), 
même si les contraintes de saturation sont respectées avec un pas de temps plus important 
(Voir \cref{im:wellsaturation01B}).
En raison de faible non-linéarité du cas du test (écoulement incompressible, perméabilité uniforme, 
courbes de perméabilité relative linéaire), la performance du {\tt pSwCoupledFoam} est
comparable à la performance d’une approche totalement implicite.

%The value of $\Delta t$ is not the only meter to compare performance of solvers: From
%\cref{im0212a}, we notice that {\tt pSwCoupledFoam} takes approximately 250 iterations to
%solve the 100-cells problem with a tolerance of $10^{-7}$. MUFITS\textregistered performs
%a total 423 iterations (Maximum of 25 Newton iterations per timeStep) and {\tt
%pSwImpesFoam} goes on to more than 1750 iterations (which is intentional as it's
%important for IMPES to perform fewer iterations per timeStep, in this case a maximum of
%only 1 iteration per timeStep is performed).
%
%By default, OpenFOAM\textregistered\relax solvers stop if either of the following conditions are
%reached:
%\begin{itemize}
%    \item The current residual is less than the solver's {\tt tolerance}.
%    \item The ratio between current and initial values of the residual is less that the
%        solver's {\tt relTol}. The current residual is then called "Final Residual".
%\end{itemize}
%\cref{im0212a} shows the gain in performance of {\tt pSwCoupledFoam} over the
%IMPES solver in similar circumstances, and presents the effects of refining the mesh for
%one extra level on both solvers performance. We notice that {\tt pSwImpesFoam} is not
%disturbed by the mesh size iterations-wise but the simulation "before breakthrough" is
%more accurate (Even though {\tt tolerance} is the same in both case). \cref{im0212b} shows
%that {\tt pSwCoupledFoam} was controlled by {\tt relTol} in the 200-cells mesh and
%presents the effect of reducing {\tt tolerance}.
%
%\begin{figure}[p]
%    \centering
%    %\input{Pictures/part1/chapter2/image10.tex}
%    \begin{subfigure}[t]{\textwidth}
%    \centering
%    \input{Data/BL/case1C/Coupled-Impes-Residuals-7-legend.pgf}
%    \input{Data/BL/case1C/Coupled-Impes-Residuals-7.pgf}
%    \caption{Coupled and IMPES solver residuals in cases with {\tt relTol}=0.1 varying
%    mesh size}
%    \label{im0212a}
%    \end{subfigure}\\
%    \begin{subfigure}[t]{\textwidth}
%    \centering
%    \input{Data/BL/case1C/Coupled-Impes-Residuals-200-legend.pgf}
%    \input{Data/BL/case1C/Coupled-Impes-Residuals-200.pgf}
%    \caption{Coupled and IMPES solver residuals in cases with {\tt relTol}=0.1 varying
%    {\tt tolerance}}
%    \label{im0212b}
%    \end{subfigure}
%    \caption{Performance comparisons for the Case 1.C}
%    \label{im0212}
%\end{figure}

\section{Benchmark 02: SPE10 Dataset 01}\label{sec:benchmark02}

Ce scénario fait partie du SPE10 Comparative Solution Project, l'ensemble des données 01 consiste 
en un modèle 2D à deux phases (huile-gaz) avec une géométrie de section verticale 2D simple 
\autocite{christie2001tenth}. Le gaz est injecté à partir d’un injecteur situé à gauche du modèle 
et l’huile est produite à partir d’un puits situé à droite du modèle. Les paramètres de 
perméabilité relative et les propriétés du fluide sont présentés dans \cref{tab:relperm02}

Les dimensions du domaine sont 762x15.24 $m^2$ et la résolution du maillage est 100x20. 
La porosité est uniforme avec une valeur de 0.2
et la perméabilité est isotrope. Le domaine présente des bandes horizontales de haute 
perméabilité. Le rapport entre la perméabilité absolue maximale et minimale est de $10^6$. 
Le domaine est initialement complètement saturé en huile.

Seul {\tt pSwCoupledFoam} est utilisé dans ces simulations en raison du petit pas de 
temps imposé par le solveur IMPES. Les résultats de saturation sont comparés aux variations 
d'un exemple de simulation fourni sur le site Web de MUFITS
\footnote{\url{http://www.mufits.imec.msu.ru/example-spe10a.html}}
et vérifiés en comparant la production cumulative d'huile avec les résultats de Landmark pour 
le maillage plus fin présentée dans l'article original \autocite{christie2001tenth} 
pour les 3000 premiers jours aprés injection.

Dans ce cas, les valeurs de tolérance du solveur doivent être au plus de l'ordre de $10^{-6}$
pour obtenir des valeurs de saturation physiquement valables \autocite{redondo2017fast}. 
Les effets gravitationnels et capillaires mettent au défi la stabilité du solveur car leurs 
flux peuvent être plusieurs fois supérieurs au flux visqueux (le seul traité de manière implicite). 
\begin{table}[!b]
    \input{Tables/part1/chapter2/tab0107.tex}
    \caption{Les paramètres de permeabilité relatives pour le benchmark 02 (SPE10)}
    \label{tab:relperm02}
\end{table}

\subsection{Benchmark 02.A: Le cas original de l'SPE10}\label{sec:benchmark02A}

Un scénario OpenFOAM identique au cas SPE10 fourni par les développeurs 
de MUFITS (effets de gravité inclus) a été simulé; la production cumulée d'huile est présentée dans 
\cref{im:oilproduction02A}.

\begin{figure}[p]
    \centering
    %\input{Pictures/part1/chapter2/image10.tex}
    \begin{subfigure}[t]{\textwidth}
    \centering
    \input{Data/SPE10/case2A/cumulative-oil-legend.pgf}
    \input{Data/SPE10/case2A/cumulative-oil.pgf}
    \caption{Cumul de production d'huile pour le Benchmark 02.A
    (Résultats de Landmark extraites de \autocite{christie2001tenth})}
    \label{im:oilproduction02A}
    \end{subfigure}\\
    \begin{subfigure}[t]{\textwidth}
    \centering
    \input{Data/SPE10/case2A/logK.tex}
    \caption{La distribution de la permeabilité absolue}
    \label{im:k02A}
    \end{subfigure}\\
    \begin{subfigure}[t]{\textwidth}
    \centering
    \input{Data/SPE10/case2A/Sg.tex}
    \caption{La distribution de la saturation en eau aprés 500 jours (Hauteur de domaine
        mise à l'échelle pour une meilleure visualisation)}
    \label{im:sat02A}
    \end{subfigure}
    \caption{Configuration et résultats du Benchmark  02.A}
    \label{im:benchmark02A}
\end{figure}

Avant que le gaz atteigne le puits de production (les 600 premiers jours), il existe 
un accord parfait des résultats deux solveurs avec les résultats de référence, principalement 
le débit d'huile est pratiquement constant.

Les différences commencent à apparaître après avoir commencé à produire du gaz. 
Après 3000 jours, le solveur MUFITS commet une erreur de 2.26 \% dans la prévision de la production 
d'huile cumulative et {\tt pSwCoupledFoam} commet une erreur maximale d’environ 3 \%.

Cette différence est principalement due à la manière simplifiée dont le processus 
d’injection est actuellement modélisée: les sources de puits "explicites" (injection avec
débit imposé) sont ajoutées directement au RHS du système matriciel; aucune résolution pour
BHP n'est effectuée (ce qui est préférable dans les logiciels totalement implicites).

La modélisation de l'injection entraine MUFITS et {\tt pSwCoupledFoam} de reporter le temps 
requis par le gaz pour atteindre le puits de production différemment (la différence est 
d'environ 20 jours). Comme il apparaît, MUFITS traite la pression dans l'\cref{eq:wellrate}
d'une manière implicite (ou partiellement implicite) pour déterminer 
une BHP equivalente du puits a chaque itération.

%\subsection{Case 2B: A coefficient calibration study on the SPE10}
%
%The previous case showed an important characteristic of the developed library: The way
%injection is handled by default is the cause of getting uniform saturation profile in
%injector cells, which greatly affects the global distribution of the saturation.
%Results from using a rate-operated
%well with a no-gravity case resemble results from \autocite{franc2016benchmark} and 
%\autocite{redondo2017fast}, which significantly differ from MUFITS results.
%
%Hence, the well model was permitted to introduce coefficients for well rates going into
%each well cell and a deterministic coefficient calibration study was conducted on a no-gravity 
%version of the SPE10 case to match MUFITS saturation profile over injection well cells
%after some simulation time.  
%
%To keep things simplified, we assume these coefficients depend only on cell size and cell
%absolute permeability (neglecting the effects of capillary pressure and relative permeability 
%near the injection well). In this particular study, all cells have the same dimensions, so
%the variations are reduced to account only for permeability effects around the well.
%
%From an optimization point of view, three issues are considered:
%\begin{enumerate}
%    \item The choice of the "time" instance at which the model is calibrated for
%        is crucial: 50 days after injection starts is chosen because all well cells will
%        undergo reasonable saturation changes in this period. Matching for earlier times wouldn't be
%        accurate because some cells will still have 0 gas saturation, and lengthening the
%        duration would slow down the optimization process.
%    \item The optimization is conducted using Sandia Labs' DAKOTA\textregistered\relax software
%        using a single-objective genetic algorithm, minimizing the function object:
%        $$f(S_g) = \sum_{i=0}^{19}(S_{gi}^{foam}-S_{gi}^{mufits})^2$$
%        Where all 20 gas saturations are evaluated after (actually, around) 50 days at
%        injection cells (The reference saturations are calculated using MUFITS). 
%        The Genetic Algorithm is preferred over least squares and other gradient-based methods 
%        because of the not-so-smooth nature of the optimized problem, with a permissive convergence 
%        criterion : $10^{-3}$.
%    \item Coefficients values should sum up to 1 so the well reaches its target injection rate.
%\end{enumerate}

\subsection{Benchmark 02.B: SPE10 avec effets de capillarité}\label{sec:benchmark02B}
\begin{figure}[!t]
    \centering
    %\input{Pictures/part1/chapter2/image10.tex}
    \begin{subfigure}[t]{\textwidth}
    \centering
    \input{Data/SPE10/case2B/Sg.tex}
    \caption{La distribution de la saturation en eau aprés 600 jours (Gravité \&
    capillarité négligées)}
    \label{im:nocap02B}
    \end{subfigure}\\
    \begin{subfigure}[t]{\textwidth}
    \centering
    \input{Data/SPE10/case2C/Sg.tex}
    \caption{La distribution de la saturation en eau aprés 600 jours (
    \label{im:cap02B}
    capillarité considérée)}
    \end{subfigure}\\
    \label{im:benchmark02B}
    \caption{Résultats du Banchmark 02.B: Effets de capillarité}
\end{figure}


L'introduction d'effets capillaires dans le cas non gravitationnel a pour 
résultat une meilleure adoucissement de la saturation en gaz, ce qui a également été 
souligné par \autocite{franc2016benchmark}. Le modèle utilisé est une relation 
Brooks-Corey avec $p_{c_{0}} = 69\ 10^3 \ Pa$ et un exposant de $\alpha=0.238$.

\section{La simulation du réservoir GRDC} \label{sec:grdccoupled}

Comme un défi réaliste pour le solveur {\tt pSwCoupledFoam}, le réservoir GRDC est simulé 
à l'état sous-saturé pendant les premières années de production en utilisant les données
du débit d'huile pour le puits RDC1B.

\subsection*{Le maillage du réservoir GRDC dans OpenFOAM}

L'adressage propriétaire-voisin utilisé dans le maillage OpenFOAM empêche deux cellules 
adjacentes de ne partager qu'une partie d'un visage (ou seulement un bord). 
Ce qui rend la conversion des grids ECLIPSE (Corner GRIDS) en maillage OpenFOAM 
particulièrement difficile, surtout s'ils décrivent des failles. Toutefois, un maillage 
très similaire peut être reconstruit avec {\tt snappyHexMesh} si la configuration 
appropriée est fournie.

L'idée est de:
\begin{itemize}
    \item 
        Extraire la surface triangulé du domaine original (au format .STL).
    \item 
        Extraire les bords caractéristiques de la surface pour aider {\tt snappyHexMesh} 
        à localiser les limites de la surface. Cela permet également de conserver la forme
        géométrique du domaine.
    \item 
        Construiser un maillage de base avec {\tt blockMesh} avec le même nombre de cellules 
        dans chaque direction que la Grid d'origine.
    \item 
        Exécuter la phase {\tt castellatedMesh} de {\tt snappyHexMesh} en éliminant les 
        cellules qui sont cellules qui sont hors de la surface. 
\end{itemize}

Cette procédure construit toujours un maillage OpenFOAM valide; la différence entre le
maillage généré rt le maillage ECLIPSE d'origine 
dépend fortement de la qualité initiale de la Grid (les facteurs les plus importants 
sont la présence des faces non-manifolds et auto-sécantes). Si une correspondance précise 
avec les limites extérieures du domaine est souhaitée, la phase {\tt snap} du flux de 
travail {\tt snappyHexMesh} doit être effectuée. En fait, pour ce cas particulier, cette
phase n'est pas nécessaire ({\tt castellatedMesh} génère un maillage qui perd 0.03\% du
volume original).

Le maillage généré doit exclure certaines cellules de la grille d'origine 
(par exemple, des cellules individuelles isolées ou même des régions isolées) 
et en introduire de nouvelles (par exemple, des régions raffinées autour des 
bords de la surface), ce qui nécessite une interpolation correcte (Upscalling) 
du champ de perméabilité. Ce sujet n’est pas trivial et nous avons uniquement 
utilisé une méthode de calcul de la moyenne des volumes pour remplacer les données 
manquantes, mais l’utilitaire responsable de cette action permet d’ajouter 
facilement d’autres options.

\subsection*{Résultats de la simulation GRDC}

\begin{figure}[H]
    \centering
    \input{Data/GRDC/myP-legend.pgf}
    \adjustbox{max width=\textwidth}{
    \input{Data/GRDC/myP.pgf}
    }
    \caption{L'évolution de la pression du réservoir GRDC dans le temps.}
    \label{im:grdcpevolution}
\end{figure}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Data/GRDC/p-2500.pdf}
    \caption{La distribution de la pression aprés 2000 jours de production}
    \label{im:grdcdistribution}
\end{figure}

Les résultats de la simulation du réservoir avec {\tt pSwCoupledFoam} sont satisfaisants
(Voir \cref{im:grdcpevolution}): L'erreur relative est de 5.61\% aprés 3000 jours. La
distribution de la pression est représentée dans la \cref{im:grdcdistribution}.
