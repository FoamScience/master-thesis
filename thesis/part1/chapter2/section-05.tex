\section{Implémentation des solveurs BlackOil} \label{sec:solversimplementaion}

\subsection{ Une formulation compositionnelle généralisée pour les équations BlackOil}
\label{sec:blackoilformulation}

La formulation compositionnelle généralisée \autocite[6]{moncorge2018sequential}, qui prend en compte 
un écoulement triphasé à trois composants avec transfert de masse en interphase, a été choisie comme 
base de ce travail car elle présente de nombreux avantages par rapport aux formulations en
volumes traditionnelles \autocite[14]{el2009realtime} et d'autre modèles modifiés e.g.
\autocite[494-497]{chen2000formulations}. 
Les avantages les plus intéressants de cette formulation peuvent être résumés comme suit:
\begin{enumerate} 
    \item Simplicité d'implémentation sous forme de code OpenFOAM. 
    \item Valable pour plusieurs types de problèmes de réservoir. 
    \item La capacité de gérer éventuellement des problèmes de points de bulle variable. 
\end{enumerate}

La formulation générale se concentre sur la pression, les saturations, les propriétés et 
la composition des composants dans la phase hydrocarbonée. La conservation en masse de la composante 
hydrocarbonée $c$ s’exprime comme suit:
\begin{ceqn}\begin{align}
    \frac{\partial}{\partial t}\left(\phi (y_c\overline{\rho}_gS_g + x_c\overline{\rho}_oS_o)\right)
    + \nabla . \left(y_c\overline{\rho}_g\vec{u}_g+x_c\overline{\rho}_o\vec{u}_o \right)
    + y_c\overline{\rho}_g \overline{q}_g + x_c\overline{\rho}_o \overline{q}_o = 0
    \label{eq:blackoilformulationhc}
\end{align}\end{ceqn}
Et pour le composant eau:
\begin{ceqn}\begin{align}
    \frac{\partial}{\partial t}(\phi \overline{\rho}_wS_w )
    + \nabla . \left(\overline{\rho}_w\vec{u}_w\right)
    + \rho_w \overline{q}_w = 0
    \label{eq:blackoilformulationw}
\end{align}\end{ceqn}

Où, la vitesse de chaque phase $i$ est modélisée avec la loi de Darcy:
\begin{ceqn}\begin{align}
    \vec{u}_i = -\frac{K.k_{rp}}{\mu_i}\left(\nabla p_i - \rho_i g\ \nabla D\right)
    \label{eq:porousu}
\end{align}\end{ceqn}
Avec les contraintes:
\begin{ceqn}\begin{align}
    z_c = \frac{y_c\rho_gS_g+x_c\rho_oS_o}{\sum_i\rho_iS_i},\qquad
    z_w = \frac{\rho_wS_w}{\sum_i\rho_iS_i},\quad 
    & \mathrm{fugacity}_{c,g}(p_g,y_c) = \mathrm{fugacity}_{c,o}(p_o,x_c)\nonumber\\
    \sum_c y_c  = 1,\qquad
    \sum_c x_c  = 1,\quad \text{et} \quad
    & S_g+S_o+S_w = 1 
    \label{eq:blackoilcontraintes}
\end{align}\end{ceqn}
La dépendance de la pression capillaire en saturation de phase est supposée d'être:
\begin{ceqn}\begin{align}
    p_{cow}  = p_o - p_w, \quad \text{et} \quad
    p_{cgo}  = p_g - p_o 
    \label{eq:blackoildependencies}
\end{align}\end{ceqn}
\nomenclature[Sp]{$S$}{La saturation du composant}
\nomenclature[Sp]{$\rho$}{La masse volumique}
\nomenclature[Sp]{$\overline{\rho}$}{La masse molaire}
\nomenclature[Sp]{$q$}{Débit volumique des puits (Injection - Extraction) par unité de volume}
\nomenclature[Sp]{$\overline{q}$}{Débit molaire des puits (Injection - Extraction) par
unité de volume}
\nomenclature[Sp]{$x_c$}{Fraction molaire du composant $c$ dans la phase huile}
\nomenclature[Sp]{$y_c$}{Fraction molaire du composant $c$ dans la phase gaz}
\nomenclature[Np]{${}_c$}{Composant d'hydrocarbures}
\nomenclature[Np]{${}_o$}{Phase de huile}
\nomenclature[Np]{${}_g$}{Phase de gaz}
\nomenclature[Np]{${}_w$}{Phase aqueuse}
\nomenclature[Sp]{$K$}{La permeabilité absolue dans les équations BlackOil}
\nomenclature[Sp]{$k_{rp}$}{Permeabilité relative de la phase $p$}
\nomenclature[Sp]{$D$}{La profondeur}
\nomenclature[Sp]{$\vec{g}$}{L'accélération de la gravité}
\nomenclature[Sp]{$B_i$}{Le facteur volumétrique pour la phase $p$}
\nomenclature[Kp]{${}^{sc}$}{La valeur de la propriété de fluide aux conditions standard}

%\subsection{Considérations pratiques}
%Le choix d'une formulation en trois phases pour le modèle mathématique BlackOil est crucial, 
%même si l'objectif est de simuler un écoulement en deux phases. Il est de bon sens de faire échouer 
%un solveur eau-huile (ou au moins de mettre fin à la simulation avec une sorte d’avertissement) 
%si l’apparition d’une phase gazeuse (libre) est attendue pendant la simulation. Continuer la 
%simulation à partir de ce moment-là n'aurait aucun sens car, fondamentalement, les équations 
%résolues ne représenteraient plus (correctement) les phénomènes physiques. Pour cette raison, 
%{\tt pSwImpesFoam} et {\tt pSwCoupledFoam} ont accès aux classes de surveillance du point de bulle.
%
%\begin{tcbremark}[colbacktitle=green,remember as=one]{}
%Si la vérification de la pression de bulle n’est pas effectuée, nous avons constaté que les 
%solveurs ont tendance à s’exercer pendant un temps considérable avant que ne survienne une erreur
%{\tt segmentationFault} (causée par la divergence de la méthode numérique).
%\end{tcbremark}
%
L'écoulement étant supposé être en deux phases, il n'est pas nécessaire d'utiliser $x_c$
et $y_c$ n'importe où dans le code, sauf dans la classe de détection de troisième phase appropriée 
({\tt blackoilFlash}); ainsi, les équations originales 
\cref{eq:blackoilformulationhc,eq:blackoilformulationw} sont
réécrites en dimensions $\mathrm{Time}^{-1}$ (en fonction des caractéristiques du solveur), 
en simplifiant les volumes molaires standard:
\begin{ceqn}\begin{align}
    \overline{\rho}_i = \frac{\overline{\rho^{sc}_i}}{B_i}
    \label{eq:rhosimplification}
\end{align}\end{ceqn}
%
%Suivant l’approche de \autocite {Horgue2015}, les flux visqueux, gravitationnels et capillaires 
%sont calculés séparément puis intégrés au flux de phase total. Cela conduit à une meilleure 
%organisation du code et facilite la manipulation interne des flux. 

\subsection{La bibliothèque de support} \label{sec:supportinglibrary}

Lors du développement des {\tt pSwImpesFoam} et de {\tt pSwCoupledFoam}, des fonctionnalités ont 
été ajoutées de manière incrémentielle en étendant la bibliothèque de support pour
acquérir des nouvelles fonctionnalités. 
%Nous avons suivi une approche orientée objet qui nécessite 
%une connaissance minimale du langage C++; La plupart des parties de la bibliothèque sont construites 
%conformément aux principes de conception suivants:
%
%\begin{enumerate}
%    \item La création d'une classe virtuelle de base pour chaque famille de modèles (par exemple, 
%        {\tt relativePermeabilityModel}), qui définit comment les modèles doivent être construits et 
%        utilisés: Chaque classe de modèle doit définir au moins une méthode {\tt correct} à exécuter 
%        les calculs requis par le modèle. La classe virtuelle définit également une méthode 
%        statique {\tt New} pour construire un pointeur pour le modèle sélectionné.
%
%    \item Chaque classe de modèle hérite de la classe de base appropriée (par exemple, 
%        {\tt krBrooksCorey} hérite de la classe virtuelle {\tt relativePermeabilityModel}) qui 
%        l’enregistre dans la table de sélection RunTime (les utilisateurs pourront sélectionner 
%        le modèle si sa bibliothèque est chargé en RunTime).
%
%    \item Chaque classe de modèle redéfinit la méthode {\tt correct} pour effectuer des ajustements 
%        de modèle personnalisés: la classe {\tt krBrooksCorey} calcule toutes les perméabilités 
%        relatives et leurs dérivées de saturation dans sa fonction {\tt correct}. Notez qu'il existe 
%        également une classe {\tt tabulated} qui hérite de {\tt relativePermeabilityModel} (elle représente 
%        donc un modèle séparé) fournissant des fonctionnalités $k_r$ tabulées.
%
%    \item L'interaction des familles de modèles avec le solveur est réduite au minimum (le solveur 
%        n'a qu'à appeler la méthode {\tt correct} pour mettre à jour les variables à chaque timeStep) 
%        en enregistrant des champs importants dans le {\tt Object Registry} du maillage.
%
%    \item La partie "Well modeling" de la bibliothèque est particulièrement complexe. Et le modèle de 
%        pression capillaire {\tt none} est la classe la plus simple.
%\end{enumerate}
%
%Ces principes permettent une interaction robuste des utilisateurs et des programmeurs avec différentes 
%parties de la bibliothèque. Un programmeur peut permettre à l’utilisateur de sélectionner de manière 
%générique, par exemple, un modèle de perméabilité relative disponible en construisant un pointeur 
%à l’aide de la méthode statique {\tt New}.
%
%\begin{tcbcppcode}{solver.C}{cpp:krmodelconstruction}{linenos=true,texcomments,}
%// An auto pointer to the relative permeability model using ::New
%autoPtr<relativePermeabilityModel> krModel =
%    relativePermeabilityModel::New  
%    (
%         "krModel", // Name of model
%         transportProperties, // Dictionary file
%         phase1, // Phase with increasing $k_r$
%         phase2, // Phase with decreasing $k_r$
%    );
%
%// The exact permeability model will be selected at RunTime
%// At (and before) CompileTime, we interact with it generically
%
%// Time loop
%while (runTime.loop())
%{
%    // Recalculate relative permeability fields
%    krModel->correct();
%}
%\end{tcbcppcode}
%
%L'exemple de \cref{cpp: krmodelconstruction} montre comment le modèle de perméabilité relative est 
%construit dans {\tt pSwImpesFoam}. Il oblige l'utilisateur à définir le mot clé {\tt relativePermeabilityModel} 
%dans le fichier {\tt constant/transortProperties}, comme indiqué dans \cref{foam: krmodelselection}.
%
%\begin{tcbfoamcode}{case/constant/transportProperties}{foam:krmodelselection}{}
%relativePermeabilityModel   someModelName;
%// If someModelName, is not a valid relative permeability model,
%// OpenFOAM fails listing available models:
%
%//Unknown relativePermeabilityModel type someModelName
%//Valid relativePermeabilityModels are : 2 (BrooksCorey tabulated)
%\end{tcbfoamcode}\index{transportProperties}
%
%Le reste de cette subsection décrit les capacités mises en œuvre de différentes parties de la bibliothèque.

\subsubsection{Modélisation de la perméabilité relative} \label{sec:krmodels}

Nous avons mis en œuvre deux modèles de calcul de la perméabilité relative:
\begin{enumerate}
    \item {\tt tabulated} lit les données de perméabilité sous forme tabulée et les interpole linéairement. 
        Le modèle nécessite un sous-dictionnaire supplémentaire dans {\tt constant/transportProperties} pour 
        spécifier la stratégie de gestion des fichiers de données et des limites (Si la
        saturation est dépasse les limites de la table). Un exemple d'utilisation est donné dans 
        \cref{foam:krtab}
\begin{tcbfoamcode}{case/constant/transportProperties}{foam:krtab}{}
relativePermeabilityModel   tabulated;
krData{
    fileName "constant/kr.dat";
    outOfBounds   exit; 
} \end{tcbfoamcode}\index{transportProperties}%
    Le fichier de données de perméabilité doit adhérer à la forme suivante
    où {\tt Sw} devrait être croissante:
\begin{tcbgenericcode}{cpp}{linenos=true,}{listing:tabulatedkrdata}{.5\linewidth}
// kr data file
(
    // Sw    (krw   kro    dkrwdSw   dkrodSw)
    1e-12    (1     0      0         -3     )
    .....
);
\end{tcbgenericcode}

        \item Le modèle {\tt BrooksCorey} implémente une modification de
            \autocite{brooks1964hydrau}. 
            Il nécessite un 
            sous-répertoire de {\tt 0}, nommé {\tt 0/BrooksCorey}, où les champs spécifiés par 
            le modèle (saturations minimales, exposants, etc.) doivent être fournis:
            $$S^* = \frac{S_w - S_{wc}}{1-S_{or}-S_{wc}}$$
            \[
            k_{rw}=
            \begin{cases}
                k_{rw,max}{(S^*)}^{n_w} & \text{, si } 0 < S^* < 1 \\
                0 & \text{, si } S^* < 0 \\
                k_{rw,max} & \text{, si } S^* > 1
            \end{cases} \qquad
            k_{ro}=
            \begin{cases}
                k_{ro,max}{(1-S^*)}^{n_o} & \text{, si } 0 < S^* < 1 \\
                k_{ro,max} & \text{, si } S^* < 0 \\
                0 & \text{, si } S^* > 1
            \end{cases}
            \]
            Alternativement, on peut specifier ces coefficients dans un sous-dictionaire
            de {\tt constant/transportProperties} nommé {\tt BrooksCoreyCoeffs}.
\end{enumerate}

%Une fois l’objet du modèle est créé dans le solveur; Il existe de nombreuses façons d'extraire 
%(de consulter) les champs de perméabilité relative. La méthode recommandée consiste à utiliser 
%les méthodes d'accès constant indiquées dans \cref{cpp:retreivekr}.
%\begin{tcbcppcode}{solver.C}{cpp:retreivekr}{linenos=true,texcomments,}
%// Non-const references to fields
%const volScalarField& krw = krModel-phase1Kr();  // $k_{rw}$
%const volScalarField& kro = krModel->phase2Kr(); // $k_{ro}$
%const volScalarField& dkrwdS = krModel->phase1dKrdS(); // $\frac{dk_{rw}}{dS_w}$
%const volScalarField& dkrodS = krModel->phase2dKrdS(); // $\frac{dk_{ro}}{dS_w}$
%\end{tcbcppcode}

\subsubsection{Modélisation de la pression capillaire} \label{sec:capmodels}

Le mot clé {\tt capillaryPressureModel} dans le fichier {\tt constant/transportProperties} permet 
de sélectionner le modèle de capillarité approprié:
\begin{enumerate}
    \item Il existe également un modèle capillaire \autocite{brooks1964hydrau} qui utilise
        le même dossier
        {\tt 0/BrooksCorey} (et le sous-dictionaire {\tt BrooksCoreyCoeffs}) pour lire ses coefficients 
        (pression capillaire maximale, exposant):
        $$p_c = p_{c_0}{(S^*)}^{\alpha}$$
    \item Un modèle logarithmique en fonction de la saturation normalisée est aussi
        disponible:
        $$p_c = -p_{c_0}\log_{10}(S^*)$$
    \item Et bien sûr, l'option {\tt tabulated}, similaire au modèle de perméabilité relative. 
    \item Les utilisateurs peuvent également négliger la capillarité en choisissant {\tt none}.
\end{enumerate}

\subsubsection{Modélisation des puits} \label{wellmodels}

La modélisation des puits dans les simulateurs de réservoir n'est pas une tâche triviale.
les principaux défis sont:
\begin{itemize}
    \item La difficulté d'estimer l'indice de productivité (à l'aide de méthodes classiques) dans 
        des mailles non structurées.%, qui est surmontée dans notre bibliothèque par la mise en œuvre 
        %d'une méthode permettant de calculer l'indice de puits pour des puits non conventionnels
        %en mailles arbitraires décrites par \autocite{wolfsteiner2003calculation}. 
    \item La nécessité des mécanismes de commutation entre différents modes de contrôle
        (Débit fixé, BHP fixée). La bibliothèque n'est pas encore prés pour exécuter ces opérations.
    \item La précision d'écoulement dans la région proche du puits est généralement tolérée par 
        la plupart des modèles.
\end{itemize}

Tout bien considéré, la présence d’une bibliothèque sophistiquée pour traiter les puits est impératif; 
Au moins, if faut que les puits supportes deux modes de contrôle:

\begin{enumerate}
    \item Puits opérés par débit: le débit est fourni explicitement sous forme de série
        temporelle.
    \item Puits opérés par BHP: une valeur pour la BHP est donnée et le débit est donc
        représenté par \cref{eq:wellrate}.
\begin{ceqn}\begin{align}
    q_i = 
    \left(\mathrm{PI}\frac{k_{ri}}{\mu_iB_i}\right)
    \left(\mathrm{BHP}_i+\rho_ig\left(D-D_{bh}\right)\right)
    - \left(\mathrm{PI}\frac{k_{ri}}{\mu_iB_i}\right)p_i
    \label{eq:wellrate}
\end{align}\end{ceqn}
\end{enumerate}

Seul les puits verticaux sont supportés par la bibliothèque.
Un tel puits est représenté par un sous-dictionnaire sous le mot clé 
{\tt wells} dans le fichier {\tt constant/wellProperties} (un exemple d'un puits vertical 
opéré par BHP est présenté dans \cref{foam:wellprops}).
Le {\tt wellModel} calcul
les contributions de tous les puits aux matrices principales et gère la commutation de mode 
de fonctionnement du puits. Au lieu 
d'utiliser des fonctions de Dirac, les contributions de puits sont divisées en quatre (4) 
champs, définis dans toutes les cellules du maillage. 
\begin{tcbfoamcode}{case/constant/wellProperties}{foam:wellprops}{}
wells 
(
    PROD0 // Nom du puits
    {
        controlMode         BHP;             // ou totalFlowRate
        operationMode       production;      // ou injection
        orientation         isoVertical;     // puits vert., milieu isotrope
                                             // ou anisoVertical
        skin 0;
        radius              0.2;             // Rayon du puits
        datumDepth          0;
        BHP                 1e6;
        perforations                        // Intervalle(s) des perforations 
        (
            cylinderToCell                  
            {
                p1    (5 99 5);             
                p2    (5 100 5);           
                radius 0.3;               
            }
            labelToCell                  
            {
                value  (0 1 2);             // Sélectionner les cellules 0,1 \& 2 
            }
        );
    }
);
\end{tcbfoamcode} \index{wellProperties,well}%

L'idée est d'exprimer
les termes sources du puits pour la phase $i$ comme suit:
$$q_i = S_{pi}p + S_{ui}$$
où $S_{ui}$ représente le champ dont les valeurs sont:
\begin{itemize}
    \item Le débit unitaire du puits, si la cellule appartient à un puits opéré par débit. 
    \item La partie indépendante de la pression dans \cref{eq:wellrate} divisée par la volume
        de la cellule, si la cellule appartient à un puits exploité par BHP.
    \item Sinon, la valeur du $S_{ui}$ est nulle.
\end{itemize}
et $S_{pi}$ est utilisé uniquement dans les puits exploités par BHP. L'algorithme suppose que 
chaque cellule appartient à au plus un puits et prend en charge le partage des cellules de 
puits en plusieurs processeurs (parallel-computing).

Les puits verticaux sont modélisés avec les types {\tt isoVertical} et {\tt anisoVertical}. 
Ils représentes fondamentalement la formulation classique de Peaceman des indices de puits 
dans les puits verticaux \autocite[15-16]{el2009realtime}:
$$r_e = \frac{
    0.14 \sqrt{{\Delta x}^2\sqrt{\frac{K_{yy}}{K_{xx}}} + {\Delta
    y}^2\sqrt{\frac{K_{xx}}{K_{yy}}}}
}
{
    0.5\left(\sqrt[4]{\frac{K_{xx}}{K_{yy}}}+ \sqrt[4]{\frac{K_{yy}}{K_{xx}}}\right)
}$$
$$\text{PI} = \frac{2\pi \Delta z\sqrt{K_{xx}K_{yy}}}{ln(\frac{r_e}{r_w}+skin)}$$
\nomenclature[Sp]{$r_e$}{Well equivalent radius}
\nomenclature[Sp]{$r_w$}{Well radius}
Ce modèle n'est valable que si toutes les cellules de puits sont des 
cuboïdes rectangulaires alignés verticalement.

        %\item La manière dont OpenFOAM gère la connectivité du maillage (ower-nighbor) rend difficile la 
        %    récupération de tailles de cellules précises dans différentes directions ($ \Delta x $, $ \Delta y $ 
        %    et $ \Delta z $). Pour remédier à cela, une orientation de puits générique est implémentée sous 
        %    la forme d'une version simplifiée de \autocite{wolfsteiner2003calculation}. L'idée est d'estimer le 
        %    PI de puits en utilisant la pression de référence (test) ($p^{ref}$) et le
        %    débit de référence ($q^{ref}$) en résolvant l'équation de diffusivité de
        %    pression dans les cellules de puits (pour l'instant, seule l'équation pour la
        %    phase huile est résolue dans le puits): 
        %    $$\frac{\partial p}{\partial t} + \nabla. (T_ {ro} \nabla {p}) = 0$$ 
        %    Cette équation est extrêmement efficace à résoudre car elle est résolue
        %    uniquement dans un sous-maillage ne contenant que les cellules du puits (généralement moins de 
        %    100 cellules). L'implémentation actuelle interdit l’utilisation de ce modèle avec des puits 
        %    monocellulaires et oblige les utilisateurs à créer un sous-répertoire dans le
        %    répertoire de la simulation pour les paramètres du sous-maillage et d’équation 
        %    ({\tt fvSchemes} et {\tt fvSolution}), dans laquelle les utilisateurs doivent spécifier le 
        %    schéma du laplacien et le solveur linéaire à utiliser.

        %    Le PI est estimé en utilisant $$\text{PI} = \frac{q^{ref}}{p-p^{ref}}$$


\subsection{L'implémentation du solveur {\tt pSwImpesFoam}} \label{sec:impessolver}

Le solveur présenté dans cette section simule,en 3D, un écoulement 
isotherme, immiscible, incompressible, sous-saturé, et non réactif de deux phases BlackOil,
à travers un milieu poreux, négligeant la compressibilité de la roche. L'un des objectifs 
de ce solveur est de servir comme une base à un développement ultérieur. 
L'algorithme implémenté par ce solveur est visualisé 
dans \cref{im:impesalgorithm}.

\begin{figure}[p]
    \centering
    \input{Pictures/part1/chapter2/image07.tex}
    \caption{L'algorithme implémenté par {\tt pSwImpesFoam}.}
    \label{im:impesalgorithm}
\end{figure}

\subsubsection{Les équations résolues}  \label{sec:impeseqn}

La méthode IMPES a été initialement développée par \autocite{sheldon1959one} et 
\autocite{stone1961analysis}. L’idée de base de cette méthode classique de résolution d’un système 
couplé différentiel partiel pour un écoulement biphasique en milieu poreux est de séparer le calcul 
de la pression de celui de la saturation. Le système couplé est divisé en une équation de 
pression et une équation de saturation, qui sont résolues 
en utilisant des approches d'approximation temporelle implicite et explicite, respectivement. La 
méthode est simple à configurer et efficace à mettre en œuvre et nécessite moins de mémoire que 
d’autres méthodes telles que la méthode de solution simultanée (SS) \autocite{douglas1959method}. 
Cette approche est encore largement utilisé dans l'industrie pétrolière (avec quelques modifications). 
Cependant, pour qu’elle soit stable, cette méthode classique nécessite de très petits pas de temps 
pour la saturation. Cette exigence est prohibitive, en particulier pour les problèmes d’intégration 
à long terme et pour les problèmes de petits tailles des cellules du maillage, tels que les 
problèmes de "coning".
%// Storage term $\phi\frac{\partial S_w}{\partial t}$ , incompressibilité de roche
%porosity*fvm::ddt(Sw)
%// Diffusivity, Intégration explicite du flux $ \nabla . ( \vec{u}_w ) $

\begin{enumerate}
    \item L'équation de saturation: est fondamentalement \cref{eq:blackoilformulationw} avec l'hypothèse
        $B_w=1$, traité explicitement. \cref{eq:blackoilformulationw} est divisé par $\overline{\rho^{sc}_w}$ et 
        donc exprimé en dimensions $\mathrm{Time}^{- 1}$. Dans le code OpenFOAM, l’équation est représentée 
        comme indiqué dans \cref{cpp:SwEqn}.
\begin{tcbcppcode}{SwEqn.H}{cpp:SwEqn}{linenos=true,texcomments}
fvScalarMatrix SwEqn
(
    // Terme de stockage, roche incompressible: $\phi \frac{\partial S_w}{\partial t}$;
    porosity*fvm::ddt(Sw)
    // Intégration explicite du flux: $\nabla\cdot(S.\vec{u}_w)$;
    + fvc::div(phiw)
    // Contribution des puits, "explicite" $q_w = S_{pw}p_w + S_{uw}$;
    + Suw + Spw*(p-pcow)
);
// Résoudre avec un solveur diagonal
SwEqn.solve(SwSolver);
\end{tcbcppcode}
    L'équation est stockée sous la forme d'une {\tt fvScalarMatrix} pour permettre à l'utilisateur 
    de choisir le schéma de temps ({\tt fvm::ddt}). Mais le solveur linéaire est
    choisis pour être "diagonal" et ne peut pas être changé au moment de l'exécution.
    \item L'équation de pression: est la somme de \cref{eq:blackoilformulationhc} pour le composant huile 
        et de \cref{eq:blackoilformulationw}, 
        chacune en dimensions $\mathrm{Time}^{-1}$, en supposant que:
        $B_w = B_o = 1,\  y_o = 0 \text{ et } x_o = 1$,
\begin{tcbcppcode}{pEqn.H}{cpp:pEqn}{linenos=true,mathescape=true,escapeinside=||}
fvScalarMatrix pEqn
(
    fvm::laplacian(-Trf, p) // Diffusivité, $-\nabla \cdot ((T_w+T_o)\nabla p)$   
    + fvc::div(phiG)        // Gravité, $\nabla\left((\rho_wT_w+\rho_oT_o)g\nabla z\right)$   
    + fvc::div(phiPc)       // Capillarité: $\nabla \cdot (T_w\nabla p_{cow})$   
    // Contribution des puits:
    + Suw + fvm::Sp(Spw, p)  // Source d'eau (semi-implicite)  
    + Suo + fvm::Sp(Spo, p)  // Source d'huile (semi-implicite)  
);
// Résoudre en utilisant un solveur itératif choisi en RunTime
pEqn.solve();
\end{tcbcppcode}
        traitant la pression implicitement. On fait la somme des deux equations 
        pour ces deux objectifs:
        \begin{itemize}
            \item Éliminer le terme de stockage dans l’équation d'huile car
                $\frac{\partial S_o}{\partial t} = -\frac{\partial S_w}{\partial t}$
            \item Rendre l’équation plus diagonalement-dominante en ajoutant une matrice diagonale.
        \end{itemize}
        Dans le code OpenFOAM, l’équation est représentée comme indiqué dans
        \cref{cpp:pEqn}.
        Les termes sources sont traitées de manière semi-implicite uniquement dans l'équation 
        de pression; ainsi, tous les puits exploités par BHP sont "automatiquement" convertis en
        puits opérés par débit dans l'équation de saturation.
\end{enumerate}

\subsubsection*{La sélection du pas du temps} \label{sec:impesdeltat}
\nomenclature[Ap]{CFL}{La condition de Courant–Friedrichs–Lewy}

Pour déterminer les limites de la taille du pas de temps IMPES, nous avons constaté que l'approche 
adoptée par \autocite{Horgue2015} est la meilleure approche (efficace et facile à implémenter)
%\footnote{
%\autocite{Horgue2015} a tiré profit de notre travail: nous avons corrigé quelques "bugs"
%et suggéré des améliorations pour leur code, qu'ils ont appliqué instantanément. Voir 
%\url{https://github.com/phorgue/porousMultiphaseFoam/issues/7} 
%}

\begin{enumerate}
    \item Un changement maximal de saturation par cellule défini par l'utilisateur entre deux temps 
        consécutifs est respecté. Cela sert d’assurer la stabilité dans les cellules avec sources de 
        puits.
        $$\Delta t_{\Delta S_{max}} = \frac{\Delta S_{max}\phi}{q_w-\nabla \cdot \vec{u}_w}$$
    \item Une méthode CFL classique est appliquée si le changement maximal autorisé de saturation 
        défini par l'utilisateur n'est pas assez petit pour assurer la stabilité de la méthode numérique. 
        \autocite{Horgue2015} a implémenté les méthodes utilisant les nombres du Courant,
        du Todd et du Coats pour déterminer un coefficient de réduction pour la taille du
        pas du temps. Nous avons choisi de n'implémenter que la procédure basée sur 
        \autocite{coats2003impes}:
        $c = \frac{C_m}{\text{CFL}}$ ou
        $$
        \text{CFL} = max_{cells} \left[ 
            \frac{\Delta t}{\phi V_{cell}}
            \left(
                    2 \frac{\partial p_{cow}}{\partial S_w} 
                        \frac{k_{rw}k_{ro}}{k_{rw}\mu_{ro}+k_{ro}\mu_{rw}}
                    \sum_{faces}\left(\frac{K_f.|\vec{S_f}|}{\Delta x_f}\right)
                +
                    \frac{\partial F_w}{\partial S_w}
                    \sum_{faces} flux_{t}
            \right)
        \right]
        $$

        Ce qui considère les effets capillaires et d’inertie, avec:
        $$F_w = \frac{k_{rw}/\mu_{w}}{k_{rw}/\mu_{w}+k_{ro}/\mu_{o}}, \qquad \text{le
        coefficient de débit fractionnaire}$$ et $flux_t$ est le flux total (eau et huile).
\end{enumerate}

La valeur du pas suivant est alors choisie comme la plus petite des valeurs:
$$\Delta t^{old} \text{min} \left(\text{min}(c, 1+0.1c),\ 1.2\right) \text{ et } \Delta
t_{\Delta S_{max}}$$

Une valeur de CFL inférieure à 2 donne des schémas explicites stables 
(avec des oscillations potentielles si 1 < CFL <2; tandis que $C_m$ est choisi comme étant
$\leq$ 1.

\subsubsection*{Suivi des erreurs de continuité} \label{sec:continuity}

Il est très important d'évaluer la validité de l'approximation (discrétisation et de résolution 
du système algébrique) sous les hypothèses émises par le solveur. 
Une approche courante dans les solveurs OpenFOAM consiste à surveiller les "erreurs de continuité", 
c'est-à-dire à sommer les flux sur toutes les faces du domaine et comparer la somme à 0. Cette approche est 
appliquée au flux total (eau-huile) dans {\tt pSwImpesFoam}.  

Un exemple de sortie du solveur est présenté ci-dessous:
\begin{tcbshellcode}{case}{linenos=true,}{}
    {% Coammand results if any
    \scriptsize 
    ..... \\
    Time = 3.0239e+08\\
    Saturation water  Min = 0.677855 Max = 0.79584\\
    PCG:  Solving for p, Initial residual = 6.03608e-06, Final residual = 6.03608e-06, No
    Iterations 0\\
    time step continuity errors : sum local = 6.71626e-06, global = 0, cumulative =
    -1.60585e-18\\
    Coats Number mean: 0.0036948 max: 0.00697812\\
    deltaT = 3392.23\\
    ExecutionTime = 358.92 s  ClockTime = 361 s\\
    \\
    wellFlowRate: PROD0 9.55288e-06 3.4675e-07\\
    .....
    }% 
    {sh:impesoutput}
pSwImpesFoam \end{tcbshellcode}

Il existe également une version anisotrope du solveur ({\tt anisoPSwImpesFoam}), qui traite 
la perméabilité absolue comme un tenseur. Le solveur lui-même ne fait aucune hypothèse,
mais les modèles anisotropes des puits assument un tenseur diagonal de permeabilité:
$$
K = 
\begin{bmatrix}
    K_{xx} & 0      & 0\\ 
    0      & K_{yy} & 0\\ 
    0      & 0      & K_{zz}
\end{bmatrix}
$$

\subsection{L'implémentation du solveur {\tt pSwCoupledFoam}} \label{sec:coupledsolver}

Ce solveur est une amélioration de {\tt pSwImpesFoam} en termes de performances et de fonctionnalités PVT: 
Il permet des pas de temps plus longs et prend en compte la compressibilité de la roche et la 
dépendance des propriétés du fluide (viscosité et densité) de la pression.

En introduisant la compressibilité des fluides (Les facteurs $B_o$ et $B_w$), nous perdons la 
possibilité d'éliminer le terme de saturation dans l'une des équations principales en les 
additionnant. Ce solveur résout, donc, directement les deux équations de BlackOil (Saturation
d'eau, pression de l'huile).

\subsubsection{Technique de résolution en blocs} \label{sec:blocksolvers}

Au lieu d'utiliser des algorithmes ségrégués (tels que IMPES) pour résoudre des systèmes 
des équations, on désire de résoudre tous les equations en même temps. Le défit le
plus important des algorithmes ségrégué est le couplage explicite, où les variables de la 
solution doivent être sensiblement sous-relâchées pour assurer la stabilité numérique. 

Le couplage implicite introduit une manière simultanée de résoudre les équations. Toutes les 
équations sont considérées comme faisant partie d'un seul système ayant une structure en blocs, 
et toutes les équations du bloc sont résolues ensemble.
Il s’agit essentiellement d’une tentative pour surmonter la 
dépendance lineaire des opérateurs d’équation d'une ( ou plusieurs de) autre variable
(que la variable de l'equation); mais pas la dépendance des coefficients d'équation sur la variable 
résolue. Par exemple, il est possible de traiter le terme $\nabla \cdot (T_{rw}\nabla p)$ dans l’équation de saturation 
(traité explicitement dans IMPES) d'une manière implicite dans un système de blocs; 
mais la dépendance $B_w = f (p)$ est toujours résolue à l'aide d'une table PVT.

Considérons un système biphasique sous-saturé formé de l'\cref{eq:blackoilformulationhc} pour 
le composant d'huile et de l'\cref{eq:blackoilformulationw}:
\tikzstyle{na} = [baseline=-.5ex]

\begin{ceqn}\begin{align}
\frac{\partial
    }{\partial t}(\frac{\phi}{B_w}S_w)
+
\tikz[baseline]{\node[fill=red!30,anchor=base] (tCPP) {$\nabla\cdot (-T_{rw} \nabla p)$};}
+
\nabla\cdot (T_{rw} \nabla p_{cow})
+
\nabla\cdot (\gamma_w \nabla z)
+
\tikz[baseline]{\node[fill=pYellow!50,anchor=base] (tPP) {$\frac{Q_w}{B_w}$};}
= 0 & \qquad \text{SEqn} \label{eq:impessaturation} \\
    \tikz[baseline]{\node[fill=red!30,anchor=base] (tL) {$-\frac{\partial
    }{\partial t}(\frac{\phi}{B_o}S_w)$};}
+
\nabla\cdot (-T_{ro} \nabla p)
+
\nabla\cdot (T_{ro}\rho_og\nabla z)
+
\tikz[baseline]{\node[fill=pYellow!50,anchor=base] (tPP) {$\frac{Q_o}{B_o}$};}
= 0 & \qquad \text{pEqn}\label{eq:impespressure} 
\end{align}
\end{ceqn}

Si nous considérons que \cref{eq:impessaturation} est l’équation de saturation, 
et que \cref{eq:impespressure} est l’équation 
de pression; on remarque qu'il existe par exemple le terme $\nabla\cdot (-T_ {rw} \nabla p)$ dans l'équation 
de saturation qui représente l'effet de la valeur de pression dans la cellule et dans ses voisins sur 
le changement de valeur de saturation dans la cellule elle-même (Même remarque pour
la source semi-implicit des puits dans l'équation de saturation et du terme temporel dans
l'équation de pression). 
La technique utilisée dans {\tt pSwCoupledFoam} a été spécialement conçue pour résoudre telles situations 
(couplage linéaire entre les équations).

L'idée est de résoudre le système tout en gardant intact le schéma de "sparesness" des équations afin 
que les solveurs linéaires courants peuvent fonctionner. Pour ce faire, nous construisons une matrice 
"bloc" $A$ où les coefficients sont des tenseurs (ici, des matrices carrée de taille 2):
\begin{ceqn}\begin{align}
    a_{P,N} = 
    \begin{bmatrix}
        a_{Sw_P,Sw_N} & a_{Sw_P,p_{PN}}\\
        a_{p_P,Sw_{PN}}& a_{p_P,p_N}
    \end{bmatrix}
    \label{eq:coupledmatrix}
\end{align}\end{ceqn}
Avec
\begin{itemize}
    \item $a_{Sw_P, Sw_N}$ est l’influence des saturations des cellules voisins sur la
        saturation de la cellule $P$. 
    \item $a_{Sw_P, p_ {PN}}$ est l’influence des valeurs de pression des voisins et de
        cellule sur la saturation de la cellule. 
    \item $a_{p_P, Sw_{PN}}$ est l’influence des valeurs de saturation des voisins et de
        la cellule sur la pression de la cellule. 
    \item $a_{p_P, p_N}$ est l’influence des valeurs de pression des voisins sur 
        la pression de la cellule.
\end{itemize}

Ensuite, nous résolvons le système $A.x = b$ où la variable de travail $x$ et le RHS $b$ 
sont maintenant des quantités vectorielles:
$$ x_P = \begin{pmatrix} Sw_P\\ p_P \end{pmatrix} 
\qquad b_P = \begin{pmatrix} \text{SEqn}_{source}\\ \text{pEqn}_{source}\end{pmatrix}$$

Cette idée s’inspire des travaux de \autocite{cardiff2016block} et de 
\autocite{jareteg2012block} et a donné des résultats satisfaisants.

\subsubsection{La sélection de la valeur {\tt timeStep}} \label{sec:coupleddeltat}

Pour la plupart des problèmes BlackOil, le traitement implicite de la saturation permettra d'obtenir des 
pas de temps plus longs. {\tt pSwCoupledFoam} décide 
la valeur {\tt timeStep} en choisissant la plus petite valeur parmi:
\begin{enumerate}
    \item 
        Une estimation initiale calculée à partir de l'équation de saturation à l'aide de
        la valeur $\Delta S_{max}$ fournie par l'utilisateur (identique à {\tt pSwImpesFoam}, 
        pour traiter correctement les cellules des puits).
    \item 
        Le résultat de la CFL IMPES, multiplié par un facteur fourni par l'utilisateur.
    \item 
        La différence entre la valeur temporelle actuelle et la prochaine fois qu'un puits
        à débit imposé modifie son débit. Ceci est crucial pour une simulation précise des cas où 
        les débits imposés de puits changent avec le temps.
\end{enumerate}

\subsubsection{Contrôle de convergence avec les CNVs} \label{sec:cnv}
\nomenclature[A]{CNV}{Convergence Normalized Value}

Parmi les règles du choix du $\Delta t$, la seule mesure qui influence directement la 
convergence des itérations est le choix de 
la valeur $\Delta S_{max}$. Les autres règles assurent simplement la stabilité des solveurs. 
Cependant, nos expériences numériques ont montré peu d'effet de $\Delta S_{max}$ sur les 
erreurs de continuité (critère par défaut dans OpenFOAM pour la convergence), 
par conséquent, $\Delta S_{max}$ doit être choisi de telle sorte que les nombres CNV 
\autocite[291]{eclipse2014techdisc}
restent toujours inférieurs à 0.001 si la convergence est désirée
(pris en charge uniquement par {\tt pSwCoupledFoam}, car {\tt pSwImpesFoam} ne peut pas 
atteindre des pas de temps suffisamment grands pour dépasser ces contraintes).

Les nombres CNV sont calculés à partir des résidus locaux des equations et du volume 
des pores en utilisant la relation suivante:
\begin{ceqn}\begin{align}
    \mathrm{CNV}_w & = B_w\ \Delta t\ \mathrm{max}|\frac{R_w}{\phi.V}|\\
    \mathrm{CNV}_o & = B_o\ \Delta t\ \mathrm{max}|\frac{R_o}{\phi.V}|
    \label{eq:cnv}
\end{align}\end{ceqn}

\subsubsection{Avantages et inconvénients} \label{sec:coupledadvantages}

L'avantage recherché avec un système d'équations couplées, comme illustré ci-dessus, est 
de parvenir à une convergence plus rapide du problème. Comme la matrice couplée nouvellement 
formée sera plus grande, le système prendra plus de temps à résoudre, mais si moins d'itérations 
sont nécessaires, le système plus grand peut toujours être bénéfique.

L’un des principaux avantages de l’approche par blocs réside dans le fait que la structure de la 
le dispersion des coefficients de la matrice ne changera pas. La matrice n'aura pas plus d'éléments,
bien que chaque élément soit constitué d'un tenseur. La nouvelle formulation pourrait modifier le 
conditionnement de la matrice et les propriétés de convergence autres que le taux de convergence. 
Il n’est donc pas certain que cette approche fonctionne pour toutes sortes de problèmes et de 
discrétisations de l’espace.

