\section{La structure d'une simulation OpenFOAM} \label{sec:openfoamstructure}

Un cas simulation OpenFOAM est un répertoire de fichiers, organisés en sous-répertoires. 
Certains fichiers sont utilisés pour configurer et contrôler la simulation, d'autres 
sont utilisés pour stocker les données résultantes de la simulation.

Pour décrire un certain problème de physiques, un dossier doit être configuré d'une 
manière prédestinée et contenir au moins trois répertoires: 
Un dossier {\tt constant}, un dossier {\tt system} et un dossier de temps initial
(généralement nomé {\tt 0}).

\subsection{Le répertoire {\tt 0} }

Ce dossier contient des fichiers décrivant les conditions initiales et les conditions
limites des variables utilisées. 
Pour les solveurs de blackOil, la porosité du domaine, les propriétés des fluides, 
leurs saturations et la variable de pression principale sont nécessaires. 

Trois entrées doivent être fournis pour chaque fichier initial d'une variable:
La dimension de la variable, le champ interne (Liste des valeurs de la variable dans les
centres des cellules, via {\tt internalField}) et les conditions de limite aux bornes du
domaine (via {\tt boundaryField}).

\begin{figure}[tb]
    \centering
    \input{Pictures/part1/chapter2/image01.tex}
    \caption{La structure d'un répertoire pour une simulation OpenFOAM typique}
    \label{im:openfoamsimulation}
\end{figure}

\subsection{Le répertoire {\tt system} }

C'est un répertoire pour définir les paramètres associés à la procédure de solution des
equations elle-même. Il contient au moins les 3 fichiers suivants: 

\subsection*{Le fichier {\tt fvSchemes} }


Dans le fichier {\tt fvSchemes}, les schémas de discrétisation numérique des différents
termes des équations modélisées sont attribués. Pour la discrétisation dans le temps, les
schémas du \cref{tab:timeschemes} sont disponibles. Ils sont attribués sous le dictionnaire {\tt ddtSchemes}.

\begin{table}[tb]
    \input{Tables/part1/chapter2/tab0101.tex}
    \caption{Les schémas de discrétisation temporelle disponible en OpenFOAM}
    \label{tab:timeschemes}
\end{table}

Les termes laplaciens, traités avec le dictionnaire {\tt laplacianSchemes}, nécessitent
\begin{itemize}
    \item Un schéma d'interpolation  pour 
        l'interpolation des coefficients de diffusion sur les centres des faces, mais, 
        en ingénierie de réservoir, il est important d'interpoler correctement les coefficients 
        de transmissibilité ce qui est géré explicitement au niveau du solveur:
        L'utilisateur est forcé à spécifier les schémas d'interpolation appropriés pour des champs
        spécifiques sous {\tt interpolationSchemes}.
    \item Un schéma pour le calcul du gradient normal à la surface (comme l'intégration gaussienne est 
        utilisée en FVM.
        \nomenclature[AO]{FVM}{Finite Volume Method (Méthodes des volumes finis)}
\end{itemize}

Une entrée décrivant un schéma linéaire par défaut avec une correction non orthogonale 
pour tous les termes laplaciens dans les équations résolues est la suivante:

\begin{tcbfoamcode}{case/system/fvSchemes}{laplacianScheme}{}
laplacianSchemes
{
//  <laplacianTerm>  <Gaussian Integration>   <iterpolation>   <snGrad>;
    default          Gauss                    linear           corrected;
}
\end{tcbfoamcode}\index{boundaryField!movingWall}%

\begin{table}[tb]
    \input{Tables/part1/chapter2/tab0102.tex}
    \caption{Les schémas du gradient normal à la surface}
    \label{tab:sngradschemes}
\end{table}

\cref{tab:sngradschemes} présente les schémas du gradient normal à la surface disponibles. 
et \cref{tab:interpolationschemes} présente quelques schémas d'interpolation qui peuvent être utilisés.

\begin{table}[b]
    \input{Tables/part1/chapter2/tab0103.tex}
    \caption{Quelques schémas d'interpolation disponibles en OpenFOAM}
    \label{tab:interpolationschemes}
\end{table}

Les schémas implicites de divergence sont rarement rencontrés dans les équations de réservoir, 
ils sont donc ignorés ici.

\subsection*{Le fichier {\tt fvSolution} }

Dans ce fichier, les solveurs linéaires pour les équations séquentielles, les tolérances, 
le nombre maximal d'itérations et les paramètres de la relaxation des
variables et des equations sont attribués. 

Plusieurs solveurs itératifs et préconditionneurs sont disponibles en OpenFOAM, 
chacun convenant à un type spécifique d'équations, en fonction de sa symétrie.

\begin{itemize}
    \item{\bf Les solveurs linéaires:}
        \nomenclature[AO]{GAMG}{Geometric-Algebraic Multi-Grid methods}
        \nomenclature[AO]{PCG}{Preconditioned Conjugate Gradient}
        \nomenclature[AO]{PbiCG}{Preconditioned Bi-Conjugate Gradient}
        \begin{itemize}
            \item (G)AMG: Un solveur multi-grid algébrique aggloméré pour les matrices diagonallement dominantes.
            \item PCG: Un solveur à gradient conjugué préconditionné pour les matrices symétriques.
            \item PbiCG: Un solveur à gradient bi-conjugué préconditionné pour les matrices asymétrique.
            \item diagonalSolver: Pour les systèmes explicites.
            \item smoothSolver: Un solveur itératif pour les matrices symétriques et asymétriques 
                  en utilisant un lisseur sélectionné pour faire converger la solution.
        \end{itemize} 
    \item{\bf Les préconditionneurs:} 
        \nomenclature[Sp]{$M$}{La matrice de préconditionnement}
        \nomenclature[Sp]{$A$}{La matrices générée par la FVM}
        \nomenclature[Sp]{$x$}{La variable principale dans le système $A\vec{x}=b$}
        \nomenclature[Sp]{$b$}{(Right Hand Side) Le vecteur source dans $A\vec{x}=b$}
        \nomenclature{DILU}{Diagonal Incomplete-LU}
        \nomenclature{DIC}{Diagonal Incomplete-Cholesky}
        \nomenclature{FDIC}{Fast Diagonal Incomplete-Cholesky}
        \begin{itemize}
            \item diagonal.
            \item DILU: Préconditionneur de LU incomplet basé sur une diagonale simplifiée pour les matrices asymétriques. 
            \item DIC: Préconditionneur de Cholesky incomplet basé sur la diagonale, équivalent symétrique de DILU.
            \item FDIC: Une version plus rapide de DIC.
            \item GAMG: préconditionnement multi-grid généralisé géométrique-algébrique.
        \end{itemize}
    \item{\bf les solveurs linéaires en blocs:} Ils ne sont disponibles que dans la
    fourche Foam-extend.
    \nomenclature[AO]{GMRES}{Generalized Minimal RESidual method}
    \nomenclature[AO]{BiCGStab}{Stabilized Bi-Conjugate Gradient}
    \nomenclature[AO]{CG}{Conjugate Gradient}
        \begin{itemize}
            \item GMRES: Résiduel minimal généralisé préconditionné pour les systèmes couplés en blocs.
            \item BiCGStab: Une version stabilisée du BiCG pour les systèmes en blocs.
            \item Il existe également des versions du GAMG, Diagonal, Gauss-Seidel et CG pour 
            les systèmes en blocs.
        \end{itemize}
\end{itemize}

%\begin{tcbremark}[colbacktitle=green,remember as=one]{My title}
%Les matrices OpenFOAM ne sont pas dynamiques, c'est-à-dire. leur modèle de dispersion 
%est défini à la création et ne peut pas être modifié.
%\end{tcbremark}

\subsection*{Les familles de solveurs linéaires dans OpenFOAM} \label{sec:solveurslineaires}

OpenFOAM implémente deux familles de solveurs linéaires largement utilisées: 
les méthodes de sous-espace de Krylov et GAMG.

\begin{enumerate}
    \item 
    Les méthodes de sous-espace Krylov (PBiCG, PCG, GMRES, etc.) évitent les opérations matrice-matrice, 
    mais multiplient des vecteurs par la matrice et contiuent avec les vecteurs résultants. 
    En commençant par un vecteur $b$, $Ab$ est calculé, puis $A^2b$ et ainsi de suite.

    Ces méthodes sont parmi les méthodes les plus efficaces actuellement disponibles en algèbre linéaire numérique. 
    Comme les vecteurs tendent très rapidement à devenir presque linéairement dépendants, les méthodes basées
    sur le sous-espace de Krylov impliquent souvent un schéma d'orthogonalisation, tel que l'itération d'Arnoldi 
    dans le cas de GMRES.

    \item 
    Au lieu de s'appuyer sur des produits matrices-vecteurs, les solveurs multi-grid
    utilise un maillage moins fin pour générer de (bonnes) solutions de départ pour un maillage 
    plus fin. Ceci est réalisé avec un grossissement géométrique de le maillage (agglomeration géométrique) 
    ou en appliquant les mêmes principes directement à la matrice, quelle que soit la géométrie 
    (agglomeration algébrique). 
    
    Le solveur GAMG résout la matrice au niveau le moins fin en utilisant CG ou BiCCG 
    (choisi automatiquement) par défaut.
\end{enumerate}



%Au sein de GAMG, 
%le maillage est agglomeré par étapes et l'algorithme de d'agglomération peut être {\tt faceAreaPair} 
%(géométrique) ou {\tt algebraicPair}.
%
%À partir de la code-source du  GAMG, nous pouvons facilement en déduire la manière dont 
%l'agglomération est réalisée dans OpenFOAM:
%
%\begin{enumerate}
%\item Récupérer les interfaces dans le niveau le plus fin du maillage.
%\item Commencer l'agglomération à partir de {\tt faceWeights} (Aires des faces pour l'agglomération géométrique 
%    ou coefficients de matrice pour l'algèbrique).
%    \begin{enumerate}
%        \item Créer des groupes de cellules pour l'agglomération (chaque cellule doit appartenir à un groupe).
%        \item Inverser l'ordre des cellules pour potentiellement améliorer le niveau d'agglomération suivant.
%    \end{enumerate}
%\item Sauf si {\tt nCoarsestCells}, le nombre de cellules au niveau le moins fin, ou {\tt
%    maxLevel}, le nombre maximal de niveaux d'agglomeration, est atteint, passez au niveau 
%    suivant et revener à (1).
%\end{enumerate}
%
%\begin{figure}[tb]
%    \centering
%    \input{Pictures/part1/chapter2/image02.tex}
%    \caption{Example of geometric agglomeration in OpenFOAM}
%    \label{img0102}
%\end{figure}


%Par conséquent, l'utilisateur de l'algorithme doit prendre soin de choisir l'approximation 
%appropriée à la taille de maille la moins fine souhaitée en spécifiant {\tt nCellsInCoarsestLevel} 
%dans les paramètres GAMG. Une entrée typique décrivant les paramètres GAMG est montrée dans \cref{gamgsettings}.
%
%\begin{tcbfoamcode}{case/system/fvSolution}{gamgsettings}{}
%solvers // Linear solver selection
%{
%    p // Select solver for pressure field
%    {
%        solver              GAMG;
%        tolerance           1e-7;
%        relTol              0.01;
%        cacheAgglomeration  true;   // Optional, defaults to false
%        nCellsInCoarsestLevel   10; // 22500 cells in original mesh
%        agglomerator    faceAreaPair; // Select geometric agglomeration
%        mergeLevels     1; // Perform 1 level of agglomeration at a tim
%        smoother            GaussSeidel;
%    }
%}
%\end{tcbfoamcode} \index{GAMG}

Le fichier {\tt fvSolution} peut également contenir des entrées supplémentaires 
telles que des facteurs de relaxation, une cellule (ou un point) de référence et une value 
de référence pour un champ spécifique.

\subsection*{Le ficher {\tt controlDict} }

Chaque simulation doit posséder un fichier {\tt system/controlDict} qui définit toutes les 
données liées à l'exécution, y compris le temps initial (en secondes), le pas initial du temps, 
l'intervalle et la méthode d'écriture des données, et le critère utilisé pour arréter
l'execution. 

Ce fichier est également responsable du chargement global des bibliothèques dynamiques et 
de la spécification des plugins (functionObjects).  

On cite ici quelque paramètres importants:
\begin{itemize}
    \item {\bf startFrom:} {\tt firstTime} pour refaire la simulation, {\tt startTime} permet de commencer à
        un temps explicitement spécifié, et {\tt latestTime} continue la simulation.
    \item {\bf deltaT:} Pas du temps en secondes.
    \item {\bf stopAt:} {\tt endTime} pour specifier quand on arrète explicitement et
        {\tt nextWrite} arrète la simulation dés que le premier temps d'écriture des
        données est atteint.
    \item {\bf writeControl:} {\tt timeStep} écrit les données chaque {\tt writeInterval}
        pas du temps et {\tt adjustableRunTime} les écrit chaque {\tt writeInterval}
        secondes (du temps simulé).
\end{itemize}

\subsection{Le répertoire {\tt constant} }

Ce dossier contient les spécifications du maillage (pour un maillage statique), 
les propriétés des puits et des fluides. 
Selon le solveur choisi, différents fichiers doivent exister. Pour les solvers
développés dans ce travail, le fichier {\tt trasportProperties} sélectionne les modèles 
de perméabilité relative, les modèles de pression capillaire, 
et la configuration des phases.

OpenFOAM utilise un volume de contrôle centré sur la cellule pour ses calculs. Le
sous-dossier {\tt polyMesh} contient des fichiers décrivant le maillage. Ces fichiers
liste les points, les faces, leurs propriétaires (owner cells) et leurs voisins (neighbor
cells). De plus, les bornes du domaine sont données dans le fichier {\tt boundary}.
