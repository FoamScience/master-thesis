\section{La méthode des volumes finis dans OpenFOAM} \label{sec:fvm}

L'\cref{eq:transporteqn} est l'équation générale du transport, qui constitue 
une base pour les équations Black Oil 
traitées dans ce travail.

\begin{ceqn}\begin{align}
    \frac{\partial \phi}{\partial t}
    + \nabla\cdot (\vec{u} \phi)
    + \nabla\cdot (K \nabla\phi)
    + S_\phi
    = 0 
    \label{eq:transporteqn}
\end{align}\end{ceqn}
\nomenclature[Sp]{$\nabla$}{L'opérateur de Divergence}
\nomenclature[Sp]{$\vec{\nabla}$}{L'opérateur du Gradient}
\nomenclature[Sp]{$\Delta$}{La variation d'une variable.}
\nomenclature[Sp]{$\phi$}{Porosité dans les équations BlackOil, 
une variable scalaire générique dans les équations générales du transport}

Où $\phi$ est la propriété scalaire pour laquelle nous résolvons l'équation 
et $K$ est la diffusivité. Les termes de l'\cref{eq:transporteqn} de gauche à droite sont: 
terme temporel, terme convectif, terme diffusif et terme source.

Il s’agit donc de trouver une solution approximative pour une equation fortement
nonlinéaire (\cref{eq:transporteqn}, $K=f(\phi)$) sur un domaine 
bien défini, qui nécessite deux étapes principales: la discrétisation du domaine et 
la discrétisation de l’équation.

\subsection{La discrétisation du domaine} \label{sec:meshdisc}

On distingue trois types principaux de maillage: les maillages structurés, 
structurés en blocs et non structurés. La topologie du maillage et les 
optimisations d'accès correspondantes du maillage peuvent avoir un impact 
direct sur la précision et l'efficacité des opérations numériques effectuées 
par la bibliothèque numérique, ainsi que sur la mise en parallèle 
des algorithmes.


Les maillages structurés prennent en charge l'adressage direct d'un voisin 
de cellule arbitraire:
les cellules sont simplement étiquetées avec des indices croissants
dans la direction de l'axe des coordonnées.
Les maillages non structurées, par contre, n'ont pas de direction apparente,
ni dans la façon dont les cellules sont ordonnées, ni dans la façon leur
topologie est construite.

Une topologie de maillage structuré augmente la précision absolue des 
interpolations appliquées dans la FVM, mais rend le maillage moins flexible 
lorsqu'il est utilisé mailler des domaines 
géométriquement complexes: Les raffinements locaux devient 
pratiquement impossibles à réaliser, car ils doivent être propagés dans la 
direction respective dans l’ensemble du maillage structuré.

\begin{figure}[!b]
    \centering
    \input{Pictures/part1/chapter2/image03.tex}
    \caption{Raffinement de maillage structuré et non structuré dans OpenFOAM}
\end{figure}

OpenFOAM implémente une FVM de deuxième ordre de convergence avec prise en 
charge de maillages non structurés arbitraires. "Non structuré arbitrairement"
signifie qu'en plus de la topologie maillée non structurée, les cellules maillées 
peuvent être de forme arbitraire. 

Nous devons cependant admettre que les cellules non ordonnées compliquent 
la possibilité d'effectuer des opérations dans une direction spécifique 
sans exécuter des recherches supplémentaires coûteuses ni recréer localement 
les informations directionnelles. Un bon exemple de telles situations est le 
calcul de la taille des cellules dans une direction spécifique à utiliser 
avec le modèle de puits de Peaceman dans les équations de BlackOil.

La manière dont les algorithmes de la méthode numérique traitent les éléments 
de maillage est déterminée par la topologie du maillage. OpenFOAM définit sa 
structure topologique en utilisant: l'adressage propriétaire-voisin et 
l'adressage des limites.

\subsubsection*{L'adressage propriétaire-voisin} \label{sec:ownerneighbor}

L'élément le plus fondamental dans la description du maillage OpenFOAM 
est la liste explicite des points (les sommets des cellules). Les faces 
sont ensuite construites en reliant ces points. Un ensemble de faces 
connectées est utilisé pour construire une "cellule".

\begin{figure}[!t]
    \centering
    \input{Pictures/part1/chapter2/image04.tex}
    \caption{L'adressage propriétaire-voisin dans OpenFOAM}
    \label{im:ownerneighbor}
\end{figure}

Chaque face interne a une cellule propriétaire 
et une voisine (où la normale de la face pointe de la cellule propriétaire 
vers la voisine). Par définition, les faces aux bornes du domaine sont les faces
ayant uniquement une cellule propriétaire et aucun voisin.
Cet adressage particulier, en dépit d’avantages computationelles 
considérables, rend difficile la conversion d’une autre format de maillage 
en OpenFOAM (dans notre cas, les maillages Corner-Grid générés par Eclipse): 
Une face peut avoir au plus deux cellules adjacentes et ces 
cellules doivent partager la face "entière".

\cref{im:ownerneighbor} montre un sous-ensemble simplifié d'un maillage non structuré 
construit à partir de 404 points. Le maillage a 501 faces dont 99 sont des faces 
internes. On note que les cellules ne sont jamais stockées sous forme 
de "liste"; Au lieu de cela, l'accent est mis sur la face elle-même 
(Ce qui permet d'optimiser les opérations d'interpolation).

\subsubsection*{L'adressage des limites}

L'isolation des faces limites et leurs stockage à la fin de la liste des faces 
de maillage optimisent l'efficacité et la parallélisation du code en les manipulant pour 
définir les conditions aux limites physiques.

%\begin{tcbremark}[colbacktitle=green,remember as=one]{}
%Il existe également des mécanismes d'adressage supplémentaires 
%pour des schémas d'interpolation plus spécialisés.
%\end{tcbremark}

\subsection{La discrétisation des équations} \label{sec:equationdisc}

Une fois le domaine est discrétisé en volumes finis, 
des approximations sont appliquées aux termes du modèle mathématique, 
transférant les termes différentiels en opérateurs discrets.
Pour obtenir le modèle discret, \cref{eq:transporteqn} est intégré dans 
le temps et l’espace:
\begin{ceqn}\begin{align}
    \int_{t}^{t+\Delta t}
    \int_{V_P}
    \left(
    \frac{\partial \phi}{\partial t}
    + \nabla\cdot (\vec{u} \phi)
    + \nabla\cdot (K \nabla\phi)
    + S_\phi
    \right)
    \du V_P\ \du t
    = 0 
    \label{eq:integratedtransporteqn}
\end{align}\end{ceqn}
\nomenclature[Sp]{$V_P$}{Le volume du control pour la FVM}
\nomenclature[Sp]{$\Delta t$}{Le pas du temps}

\cref{eq:integratedtransporteqn} peut être représenté dans le code OpenFOAM en tant 
que matrice à coefficients scalaire 
(comme indiqué dans \cref{cpp:transporteq}).
\begin{tcbcppcode}{transportEquation.C}{cpp:transporteq}{linenos=true,}
fvScalarMatrix transportEqn
(
    // Terme temporelle
    fvm::ddt(phi) 
    // Terme convective
    + fvm::div(flux, phi)   // flux: Interpolation de U aux faces
    // Terme diffusive
    + fvm::laplacian(K, phi)  // K: diffusivité
    // Terme source
    + fvm::Sp(sp, phi) + su   // sp,su: Coefficients independents du phi
);
\end{tcbcppcode}

\cref{cpp:transporteq} suppose que le terme source $S_\phi$ dans 
\cref{eq:integratedtransporteqn} est $S_\phi = S_p \phi + S_u$, c'est à dire, on le traite 
d'une manière semi-implicite. L'espace des noms {\tt fvm} fournit des opérateurs 
implicites permettant de discrétiser \cref{eq:integratedtransporteqn}; Il existe également un espace 
des noms {\tt fvc} fournissant des versions explicites des mêmes opérateurs.

\subsubsection*{Le terme du temps} \label{sec:timedisc}

En utilisant, par exemple, le schéma d'Euler pour la discrétisation temporelle, 
avec un maillage statique, {\tt fvm::ddt(phi)} est équivalent à:
\begin{ceqn}\begin{align}
    \int_{t}^{t+\Delta t}
    \int_{V_P}
    \left(
    \frac{\partial \phi}{\partial t}
    \right)
    \du V_P\ \du t
    \longrightarrow
    V_P \frac{\phi^{n+1}-\phi^{n}}{\Delta t}
    \label{eq:timeintegration}
\end{align}\end{ceqn}
\nomenclature[Kp]{${}^n$}{Le temps précédent (valeurs connues).}

L'opérateur {\tt fvm::ddt} construit ensuite une matrice avec 
$ \frac{V_P}{\Delta t} $ comme éléments diagonaux et ajoute 
$\frac{V_P \phi^n} {\Delta t} $ au côté droit du système (RHS).

\subsubsection*{Le terme convectif} \label{sec:divergencedisc}


{\tt fvm::div} dans \cref{cpp:transporteq} intègre et applique le 
théorème de Gauss de la divergence sur le terme convectif 
dans \cref{eq:transporteqn}:
\begin{ceqn}\begin{align}
    \begin{aligned}
    \int_{t}^{t+\Delta t}
    \int_{V_P}
    \nabla\cdot (\vec{u} \phi)
    \du V_P\ \du t
    &= 
    \int_{t}^{t+\Delta t}
    \int_{S}
    \left(
        \phi \vec{u} \cdot \vec{n}
    \right)
    \du S\ \du t \\
    &\longrightarrow
    \sum_{f}\phi_f\ u_f S_f
    \end{aligned}
    \label{eq:divergenceintegrated}
\end{align}\end{ceqn}
\nomenclature[Sp]{$S$}{La surface fermée du volume de control $V_P$}
\nomenclature[Sp]{$\vec{n}$}{Un vecteur unitaire normal à la face}
\nomenclature[Sp]{$\vec{S_f}$}{Un vecteur normal à la face ou la norme est l'aire de la
face}
\nomenclature[Sp]{$\vec{\phi_f}$}{Une valeur interpolée de $\phi$ au centre de la face}

La somme sur les faces de la cellule dans \cref{eq:divergenceintegrated} dépend fortement 
de la façon dont différentes propriétés sont interpolées en faces:

\begin{enumerate}
    \item $u_f$: Ceci est simplement le "flux" a travers la face.
    \item $\phi_f$: Ceci est calculé en utilisant l’un des schémas d’interpolation 
        disponibles dans OpenFOAM (choisi par l'utilisateur au moment de l’exécution). 
        L'opération "d'interpolation sur les faces" est effectuée une seule 
        fois par "face" (et non par "cellule") en raison du mécanisme d'adressage 
        propriétaire-voisin:
        $\sum_{f}\phi_f\ u_f S_f = \sum_{f}^{owners}\phi_f\ u_f S_f - \sum_{f}^{neighbors}\phi_f\ u_f S_f$
        \item Les schémas choisis dans le fichier {\tt system/fvSchemes} expriment
            $\phi_f$ en fonction de $\phi_P$, la valeur scalaire de la cellule 
            propriétaire et $\phi_N$: les valeurs scalaires dans les cellules voisines.
\end{enumerate}

Le champ en faces $\phi_f$ peut être évalué en utilisant des différents schémas 
(Voir \cref{tab:interpolationschemes}); Une description mathématique est donnée ici pour les uns les 
plus utiles:
\nomenclature[Ap]{CD}{Central Differencing}
\nomenclature[Ap]{BD}{Upwind  Differencing}
\nomenclature[Ap]{UD}{Blended Differencing}

\begin{itemize}
    \item Interpolation linéaire (Central differencing, CD)
        $$\phi_f=f_x\phi_P+(1-f_x)\phi_N$$ avec $f_x=\frac{|C_fN|}{|PN|}$ (Voir
        \cref{im:meshquality})

    \item L'Interpolation vers le haut (Upwind differencing, UD) determine une valeur
        interpolée selon la direction d'ecoulement (le signe du flux $F$)
        $$
        \phi_f=
        \begin{cases}
            \phi_P, & \text{pour } F \geq 0\\
            \phi_N, & \text{pour } F < 0
        \end{cases}
        $$
    \item L’interpolation mixte (Blended Differencing, BD) détermine une valeur interpolée en 
        combinant UD et CD: $$\phi_f = (1-\gamma) (\phi_f)_{UD} + \gamma(\phi_f)_{CD}$$ 
        Le coefficient $\gamma$ du schéma de différenciation Gamma est choisi à l'exécution 
        et d'autres variantes sont disponibles ({\tt vanLeer}, ... etc).

    \item L'interpolation harmonique; principalement utilisée our la permeabilité absolue:
        $$\phi_f=\left(\frac{f_x}{\phi_N}+\frac{1-f_x}{\phi_P}\right)^{-1}$$
        où $f_x$ est identique à l'interpolation linéaire.
\end{itemize}

D'aprés \cref{eq:divergenceintegrated}, il est facile de déduire que l'operator {\tt fvc::div(facePhi)}
effectue une intégration gaussienne sur les faces du volume de contrôle pour le champ 
$\phi_f$, reconstruisant ainsi le champ de volume $\phi$ du champ en faces $\phi_f$. 
La contribution gravitationnelle aux équations BlackOil est traitée de cette façon.

\subsubsection*{Le terme diffusif} \label{sec:diffusiondisc}


L'operateur {\tt fvm::laplacian} discrétise le terme diffusif comme indiqué ci-dessous:
\begin{ceqn}\begin{align}
    \begin{aligned}
    \int_{t}^{t+\Delta t}
    \int_{V_P}
    \nabla\cdot (K \nabla\phi)
    \du V_P\ \du t
    &= 
    \int_{t}^{t+\Delta t}
    \int_{S}
    \left(
    K \nabla\phi
    \right) \cdot \vec{n}\ 
    \du S\ \du t \\
    &\longrightarrow
    \sum_{f}\left(K\nabla\phi\right)_f\ S_f
    \end{aligned}
    \label{eq:diffusionintegrated}
\end{align}\end{ceqn}

Les schémas  de laplacien expriment ensuite la somme sur les faces 
des cellules sous la forme $a_P \phi_P + \sum a_N \phi_N + a_0$.

\begin{tcbremark}[colbacktitle=green,remember as=one]{}
Le champ {\tt K} dans \cref{cpp:transporteq} peut être un champ centré sur 
une cellule ({\tt volScalarField} dans la terminologie OpenFOAM). 
Une interpolation aux faces $K_f$ doit donc avoir lieu afin d'évaluer la 
somme des faces dans \cref{eq:diffusionintegrated}. Mais, dans les équations du réservoir, 
la transmissibilité est un champ complex dépendant de la mobilité, 
des perméabilités absolues et relatives, qui doivent être interpolées de 
manière très spécifique; Ainsi, $K$ est correctement interpolé ensuite fourni 
comme un champ centré sur la face ({\tt surfaceScalarField}) dans tout
les solveurs qu'on développe.
\end{tcbremark}

\subsubsection*{Le terme source}\label{sec:sourcedisc}

Le terme source semi-implicit, comme dans les modèles des puits, est discrétisé 
en utilisant l'opérateur {\tt fvm::Sp}: 
\begin{enumerate} 
    \item{\tt fvm::Sp(phi)} ajoutera les valeurs du {\tt sp} sur la diagonale 
        de la matrice pour le champ {\tt phi}. 
    \item Si un terme source doit être traité explicitement, il est ajouté 
        directement au RHS ({\tt su} dans \cref{cpp:transporteq}) 
\end{enumerate}

Pour récapituler, les coefficients de l'équation discrète (structure de "sparseness" 
et valeurs des coefficients) seront déterminés par les facteurs suivants:

\begin{enumerate} 
    \item L'interpolation des valeurs centrées sur les faces (le flux, la diffusivité et 
        les variables principales) à l'aide de différentes méthodes d'interpolation. 
    \item La forme géométrique de la cellule (en particulier le nombre de faces de la
        cellule) ainsi que le nombre et la géométrie des cellules adjacentes. La forme de la 
        cellule détermine la position du centre de la cellule, et a donc un impact sur 
        l'interpolation. 
    \item La taille de la cellule: plus $V_P$ est petit, plus l'équation algébrique 
        sera proche de l'équation exacte, augmentant ainsi la précision de la solution 
        (virtuellement). 
    \item Le pas de temps utilisé: un pas de temps plus petit donne une meilleure précision 
        du temps pour les problèmes transitoires et une solution plus stable, lorsque des 
        schémas explicites sont utilisés. 
\end{enumerate}

\subsection{Les conditions aux limites (BCs)}\label{sec:fvmbc}

Tout en étendant le terme de somme sur les faces (en termes convectifs et diffusifs 
respectivement dans \cref{eq:divergenceintegrated} et \cref{eq:diffusionintegrated}), 
la sommation rencontrera une face 
de cellule qui est une face limite. Si cette face est marquée avec $b$, un terme similaire
à $\phi_b u_b S_b$ doit être calculé et ajouté à la somme. Comme il n'y a qu'une seule 
cellule à côté d'une face limite, cette cellule est - par définition - le propriétaire 
de cette face.
\nomenclature[Np]{${}_f$}{Une valeur interpolée au centre de la face.}
\nomenclature[Np]{${}_b$}{Une face de limite.}
\nomenclature[Np]{${}_P$}{Le volume de control.}
\nomenclature[Np]{${}_N$}{Un voisin (cellule en connexion avec) le volume de control.}

Les manières de prescrire les valeurs limites sont diverses, mais les méthodes les plus 
élémentaires sont {\tt fixedValue} et {\tt zeroGradient}.
Dans le maillage à volume fini, chaque face frontière appartient à un patch de frontière 
(une collection de faces aux limites du domaine) et chaque patch est défini comme 
appartenant à un certain type. Une collection de patches construit le maillage de limites, 
que OpenFOAM appelle {\tt boundaryField}.
\nomenclature[Ap]{BC}{Boundary Condition (Conditions aux limites)}

Un problème courant pour les BC est le traitement de la "non-orthogonalité" au niveau 
des faces limites, c'est-à-dire lorsque le vecteur $\vec{PC_b}$ ($C_b$ est le centre 
de la face) n'est pas orthogonal à la face (non parallèle à $\vec{S_f}$). Dans OpenFOAM, 
cette situation est traitée en définissant un vecteur $\vec{d_n}$ qui représente la 
distance orthogonale entre le centre de la cellule $P$ et la face limite, où: 
$$\vec{d_n}=\frac{\vec{S_b}}{|\vec{S_b}|}\frac{\vec{PC_b} \cdot \vec{S_b}}{|\vec{S_b}|}$$

Pour les BCs {\tt fixedGradient}, qui prescrivent le produit scalaire du gradient de la 
propriété à la limite du domaine et du vecteur normal à la face 
$$\vec{n_b} \cdot \nabla \phi(x_b) = g_a$$ 
La valeur à la limite est déduite à l'aide de l'approximation de la série de Taylor 
$\phi_b = \phi_P+\vec{d_n} \cdot \nabla \phi(x_b)$, ce qui peut etre exprimé comme
$\phi_b = \phi_P+|\vec{d_n}|g_b$.

Cette valeur de $\phi_b$ est utilisée dans la somme convective uniquement
(\cref{eq:divergenceintegrated}). 
Pour la somme diffusive, $\vec{S_b} \cdot \nabla\phi(x_b) = |\vec{S_b}|g_b$ permet l'insertion 
directe du terme $K_b|\vec{S_b}g_b|$ dans la somme. Bien entendu, aucune diffusion 
ne provient de {\tt zeroGradient}.

Dans {\tt fixedValue} BCs, $u_b$ et $\phi_b$ sont tous les deux fournis explicitement pour 
que la somme de convection soit construite. La somme diffusive a besoin du gradient de
$\phi$ au centre de la face:
$$|\vec{S_b}| \cdot \nabla \phi(x_b) = |\vec{S_b}|\frac{\phi_b-\phi_P}{|\vec{d_n}|}$$
Des différentes conditions aux limites sont implémentées dans OpenFOAM et toutes spécifient 
soit la valeur limite, soit le gradient, soit une combinaison des deux, sur un ensemble de 
faces à la frontière du domaine. Ce qui suit traite les BC les plus fréquemment rencontrés 
dans l'ingénierie des réservoirs selon un mode de codage OpenFOAM.

Les types de patchs de limites sont affectés à des "types" (voir \cref{tab:bctypes}) qui 
contrôlent les types des BC autorisés à être appliqués.
\begin{table}[tb]
    \input{Tables/part1/chapter2/tab0104.tex}
    \caption{Les types des patchs de limite dans OpenFOAM}
    \label{tab:bctypes}
\end{table}

%\subsubsection{Empty-type BCs}
%
%Pour un patch "empty" (vide), il n'y a pas de flux à travers les faces du patch; Et comme 
%OpenFOAM ne dispose pas d'algorithme spécial pour traiter les maillages 2D (ou 1D), il est 
%bon de ne rien faire pour ces faces, c’est-à-dire de les exclure de l’extension de la somme 
%sur les faces dans \cref{eq0204} et \cref{eq0205}. Bien entendu, la direction à ignorer ne 
%doit comporter qu'une seule couche de cellules. C’est ce que fait le {\tt empty} BC.
%
%\subsubsection{SymmetryPlane-type BCs}
%
%Seul {\tt symmetryPlane} BC est autorisé à être appliqué aux types de patchs {\tt
%symmetryPlane}, et il permet de s'assurer que le composant du gradient perpendiculaire 
%à la limite doit être fixé à zéro.
%
%\subsubsection{Wall-type BCs}
%
%Si un patch se voit attribuer le type "wall", il est supposé que la vitesse du fluid est 
%une sous-type de {\tt fixedValue} BC (non utilisé dans notre simulations de réservoir, mais 
%constituerait une base solide pour "la considération des aquifères d'eau") et la pression 
%aura {\tt zeroGradient} BC (barrière imperméable).
%
%\subsubsection{Cyclic-type BCs}
%
%Si une géométrie est constituée de plusieurs composants identiques (par exemple, une aube 
%d'hélice ou une aube de turbine), un seul doit être discrétisé et traité comme s'il se 
%trouvait entre des composants identiques. Pour une hélice à quatre pales, cela signifierait 
%que seule une pale est maillée (maillage à 90°) et qu'on affecte un type de patch cyclique 
%aux patchs avec des normales dans la direction tangentielle. Ces patchs agiraient alors comme 
%étant couplés physiquement. Nous avons peu d’utilisation pour ce type dans les simulations 
%de réservoir.
%
%Si aucun type de patch n'est appliquable, le type générique "patch" est utilisé.

\subsection {Utilitaires de maillage OpenFOAM}\label{sec:meshutilities}

Il existe plusieurs mailleurs open source spécialement conçus pour OpenFOAM. 
Cela inclut les utilitaires officiels {\tt blockMesh}, {\tt snappyHexMesh}, {\tt foamyHexMesh} 
et l'informel: {\tt cfMesh}. {\tt blockMesh} et {\tt snappyHexMesh} seront brièvement abordés 
dans cette section en décrivant leur utilisation et leurs principes de fonctionnement, car ils 
sont très utiles pour les tâches de simulation des réservoirs. D'une manière générale, les générateurs de 
maillage ont pour but de générer les fichiers {\tt polyMesh} décrits dans la section précédente 
(points, faces, owner, neighbor et boundary) de manière convenable.

\subsubsection{blockMesh}\label{sec:blockmesh}

Comme son nom l'indique, l'utilitaire est capable de construire des maillages structurés 
en blocs à partir de
l'information prescrite dans le fichier
{\tt constant/polyMesh/blockMeshDict}:
\begin{enumerate} 
    \item La liste des sommets des blocs (définits en coordonnées cartésiennes). 
    \item Une liste de blocs (définissant les sommets et les informations de raffinement 
        pour chaque bloc). 
    \item La liste des patchs de limites, définissant quelle face du bloc appartient à
        quel patch.
\end{enumerate}


%\begin{tcbfoamcode}{constant/polyMesh/blockMeshDict}{blockmesh}{}
%vertices
%(
%    (0   0   0) // vertex 0
%    (1   0   0)
%    (1   1   0)
%    ..........
%);
%blocks          
%(
%    // Hexagonal block of vertices 0 through 8
%    // with 1 cell in x-direction, 100 in y and 1 in z
%    // all cells have the same direction-wize size
%    hex (0 1 2 3 4 5 6 7) (1 100 1) simpleGrading (1 1 1)
%);
%boundary
%(
%    inlet // patch name
%    {
%        type patch; // empty, symmetryPlane ...
%        faces // list of block faces
%        ( (3 7 6 2) );
%    }
%    .....
%);
%\end{tcbfoamcode} \index{blockMesh}

\subsubsection{snappyHexMesh} \label{sec:snappyhexmesh}

Avec {\tt snappyHexMesh}, les maillages hexa-dominané 
peuvent être générés facilement. Ils ne nécessitant que deux conditions préalables: 
\begin{enumerate} 
    \item Un maillage d'arrière-plan (en hexagonales) sur lequel on travaille. 
    \item Une ou plusieurs géométries dans un format de surface compatible (fichiers STL ou OBJ) 
        et leurs "feature edges" (bordures caractéristiques). 
\end{enumerate}

L'exécution de {\tt snappyHexMesh} peut être divisé en trois étapes principales qui 
sont exécutées successivement. Chacune de ces étapes peut être désactivée en définissant le 
mot clé correspondant comme "false" au début du fichier {\tt system/snappyHexMeshDict}.
Ces trois étapes peuvent être résumées comme suit:
\begin{enumerate}
    \item {\bf castellatedMesh}: il s’agit de la première étape et effectue deux opérations 
        principales. Tout d'abord, on ajoute la géométrie au maillage d'arrière-plan et on 
        supprime les cellules qui ne sont pas à l'intérieur de la géométrie. Ceci est similaire 
        à {\tt ACTNUM} dans le format de grid Eclipse; la seule différence (en principe) est que 
        les cellules supplémentaires sont éliminées complètement en OpenFOAM. Deuxièmement, les 
        cellules existantes sont fractionnées et affinées conformément aux spécifications de 
        l'utilisateur. Le résultat est un maillage composé uniquement d'hexaèdres qui ressemble 
        plus ou moins à la géométrie. Si une bonne correspondance avec les grids Eclipse
        est désirée, un arrêt à cette phase est généralement suffisant pour les réservoirs. 
        Cette phase laisse la plupart des points de maillage supposés être placés sur la surface 
        de la géométrie non alignés avec elle.
    \item {\bf snap} En effectuant l’étape d’accrochage, les points de maillage situés au 
        voisinage de la surface sont déplacés vers la surface. Au cours de ce processus, 
        la topologie de ces cellules peut être changée d'hexaèdre en polyèdre. Les cellules 
        proches de la surface peuvent être supprimées ou fusionnées.
    \item {\bf addLayers} Enfin, des cellules supplémentaires sont introduites sur la 
        surface géométrique, généralement utilisées pour affiner le flux proche du mur. 
        Les cellules préexistantes sont éloignées de la géométrie afin de créer un espace 
        pour les cellules supplémentaires. Ces cellules sont susceptibles d'être des prismes.
\end{enumerate}

Les bordures caractéristiques des entités géométriques aident à détecter les bords principales 
de la surface triangulé afin que {\tt snappyHexMesh} maille le domaine comme prévu. 
Ils sont extraits à l'aide d'un utilitaire: {\tt surfaceFeatureExtract} et exportés comme un 
fichier {\tt *.eMesh}.

Les trois phases sont configurées dans le très long fichier {\tt system/snappyHexMeshDict},
en plus de la qualité du maillage de sortie. Il est recommandé de vérifier la qualité du 
maillage généré juste après sa génération à l'aide de l'utilitaire {\tt checkMesh}, qui 
effectue des contrôles de géométrie et de topologie sur le maillage et génère des statistiques 
de maillage utiles.

\subsection{La quality du maillage OpenFOAM}\label{sec:meshquality}
\begin{figure}[tb]
    \centering
    \input{Pictures/part1/chapter2/image05.tex}
    \caption{Mesures de la qualité du maillage et le traitement de la non-orthogonalité aux
    limites du domaine dans OpenFOAM.}
    \label{im:meshquality}
\end{figure}

Les différents software CFD définissent différentes métriques et normes de qualité 
du maillage.  \cref{im:meshquality} est utilisé pour illustrer différents aspects 
de l'interprétation géométrique de ces métriques de qualité en OpenFOAM:

\begin{enumerate}
    \item {\bf Non-orthogonalité de face}: C'est le produit scalaire normalizé (un angle) 
        du vecteur de zone de visage avec un vecteur allant du centre de la cellule propriétaire 
        au centre de la cellule voisine: 
        $$\mathrm{face-nonorthogonality} = \frac{\vec{S_f} \cdot \vec{PN}}{|\vec{S_f}||\vec{PN}|} $$ 
        Un bon maillage a une non-orthogonalité maximale de 75 degrés ou moins.
    \item {\bf La "skewness" de la face} est calculée comme la distance entre le centre de
        face et le point d'intersection entre le vecteur cellule-à-cellule et la face
        elle-meme, normalisée par la distance entre le centre de la cellule propriétaire 
        et le centre de la cellule voisine. L'asymetrie des faces limites peut atteindre 
        20, mais pour les faces internes, sa valeur est limitée à 4.
    \item {\bf La concavité de face} vérifie les angles intérieurs constituant la face. 
        Une face est dite convexe si l'un de ses angles internes est supérieur à 100 degrés. 
        Notez que dans \cref{im:meshquality}, l’illustration de la concavité des faces considère l'hexagone 
        entier comme "un face" et non comme une cellule (Dans les autres cas, la face est
        illustrée comme une ligne); 
        l'illustration définit P comme centre du visage. Considérez cela comme une vue de 
        dessus d'une cellule.
    \item {\bf Le déterminant des cellules} est calculé en prenant le déterminant du tenseur calculé 
        à partir des vecteurs de surface faciale. Le calcul du déterminant de cellule est utilisé lors 
        des opérations {\tt fvc::reconstruct} (par exemple, reconstruire le flux à partir du 
        champ de vitesse), de sorte qu'il doit être bien conditionné.
    \item {\bf Volume minimal de cellule} vérifie s'il y a des cellules qui sont trop petites 
        (les volumes inférieurs à $10^{-13}\ m^3$).

    \item {\bf Déformation de la face}: lorsqu'un sommet n'appartient pas au même plan que les 
        sommets de la face inférieure. Pas si important.
\end{enumerate}

\subsection{La résolution des systèmes des équations algébriques} \label{sec:algbrsystems}

Maintenant que les PDE (tels que \cref{eq:transporteqn}) sont approximées par des équations algébriques dans 
chaque cellule du maillage (si une discrétisation temporelle implicite est utilisée):
\begin{ceqn}\begin{align}
    a_P\phi_P + \sum_{voisins} a_N\phi_N = b
    \label{eq:algebraicfvm}
\end{align}\end{ceqn}

Toutes les équations par pas du temps sont assemblées dans un système:
$$A\vec{x} = \vec{b}$$

%Chaque ligne dans $A$ représente la connexion directe de la cellule particulière aux autres cellules. 
%Comme une cellule ne possède pas beaucoup de connexions directes avec les cellules restantes, seules 
%quelques colonnes de cette ligne ont une valeur. Les colonnes restantes sont remplies de zéros.

Pour les systèmes larges, les méthodes itératives tentent de résoudre
$A\vec{x}^{(k)} = \vec{b} - R^{(k)}$.
La solution intermédiaire $x^{(k)}$ après k itérations résout exactement l'équation d'origine, 
il faut donc introduire un résidu $R^{(k)}$. L’objectif est toujours de ramener le résidu à zéro. 
Puisque les solveurs itératifs ne résolvent pas le système d'équations de manière absolument précise, 
le degré de précision doit être défini d'une manière ou d'une autre. C'est ici que le résidu entre en jeu,
définissant la différence entre la solution exacte et l'itération actuelle.
\nomenclature[Sp]{$R$}{Le résiduel pour $A.\vec{x} = \vec{b}$. $R = |A.\vec{x}-\vec{b}|$}
\nomenclature[Kp]{${}^{(k)}$}{La valeur de la K${}^{ème}$ itération.}

\subsubsection{Amélioration de la convergence} \label{sec:relaxation}

La sous-relaxation permet à l'utilisateur de définir un facteur de fusion $\theta$ entre l'ancienne 
et la nouvelle solution pour contrôler le changement dans le champ lui-même ou dans l'équation. 
OpenFOAM fournit deux methodes de rélexation: Pour les champs en utilisant
$\phi^{new}=\phi^{(k)}+\theta(\phi^{(k+1)}−\phi^{(k)})$, 
ainsi que la possibilité de sous-relâcher les équations: Les solveurs linéaires exigent que 
la matrice des coefficients soit au moins diagonalement balancée, et de préférence dominante 
diagonalement, c'est-à-dire par rangée, la magnitude de la diagonale doit être supérieure ou 
égale à la somme de les composants non diagonaux; ainsi, les coefficients $a_P$, $a_N$ et
$b$ dans \cref{eq:algebraicfvm} sont modifiés de tel manière en rendant la matrice dominante en diagonale.

Les facteurs de sous-relaxation, s'ils sont utilisés, sont spécifiés dans 
{\tt system/fvSolution} au niveau de la casse. Un exemple de sous-relaxation du champ de 
pression et de l'équation de saturation en eau est présenté dans \cref{foam:underrelaxation}.

\begin{tcbfoamcode}{case/system/fvSolution}{foam:underrelaxation}{}
relaxationFactors
{
    fields
    {
        p  0.95; // Pour la pression
    }
    equations
    {
        Sw 0.98; // Pour l'équation de Sw
    }
}
\end{tcbfoamcode}\index{relaxationFactors}%

\subsubsection{ Contrôle de convergence dans les solveurs linéaires }
\label{sec:convergencecontrol}

Après chaque itération du solveur, le résidu est réévalué. 
Le solveur s'arrête si l'une des conditions suivantes est remplie 
\begin{itemize}
    \item 
        Le résidu tombe en dessous de la tolérance du solveur linéaire 
        ({\tt tolérance} dans les paramètres du solveur);
    \item 
        Le rapport entre les résidus actuels et initiaux est inférieur à la tolérance relative du solveur 
        ({\tt relTol} dans les paramètres du solveur linéaire);
    \item 
        Le nombre d'itérations dépasse un nombre maximal d'itérations, ({\tt maxIter} dans
        les paramètres du solveur);
\end{itemize}
