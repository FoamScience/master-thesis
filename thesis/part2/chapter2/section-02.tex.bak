\section{Etude PVT}\label{etude-pvt}

Des études de laboratoire précises de PVT et le comportement d'équilibre
de phase des fluides de réservoir sont nécessaires pour caractériser ces
fluides et évaluer leurs performances volumétriques à différents niveaux
de pression. Il y a de nombreuses analyses de laboratoire qui peuvent
être faites sur un échantillon de fluide du réservoir. La quantité de
données désirée détermine le nombre de tests effectués dans le
laboratoire.

\subsection{Les principaux tests du l'etude PVT}\label{les-principaux-tests-du-letude-pvt}

Les principaux tests effectués pour avoir le modèle PVT du
fluide de réservoir sont les suivant :


\begin{itemize}
    \item 
Expansion à composition constante (CCE)
    \item 
Libération différentielle (DL)
    \item 
Tests de séparateur (TS)
\end{itemize}

\subsubsection{Expansion à composition constante (CCE)}\label{expansion-uxe0-composition-constante-cce}

Le test est effectué dans le but de déterminer:
\begin{itemize}
    \item 
La pression de saturation (pression de bulle dans ce cas)
    \item 
        Coefficients de compressibilité isothermique du fluide monophasé ($P > P_b$ )
    \item 
Volume total d'hydrocarbures en fonction de la pression
\end{itemize}

La procédure expérimentale, illustrée schématiquement par la
\cref{im:1118},
implique de placer un échantillon de fluide (huile dans ce cas) dans une
cellule PVT à la température du réservoir(\cref{im:1118}, Section
A). La pression est réduite par paliers à température constante
en éliminant le mercure de la cellule et la variation du volume total
d'hydrocarbures $V_t$ est mesurée pour chaque changement de
pression.
La pression de saturation (pression de bulle dans ce cas) et le volume
correspondant correspondant sont observés et enregistrés et utilisés
comme volume de référence $V_{sat}$ \cref{im:1118}, Section
C. Le volume relatif et est exprimé mathématiquement par
l'équation suivante:
$$V_{rel} = \frac{V_t}{V_{sat}}$$
\begin{itemize}
    \item 
        $V_{rel}$: volume relatif
    \item 
$V_t$:volume total d'hydrocarbures
    \item 
        $V_{sat}$: volume à la pression de saturation
\end{itemize}

La masse volumique de l'huile est déterminée à partir des mesures
directes poids / volume effectuées sur l'échantillon dans la cellule
PVT.
La compressibilité de l'huile (pour $P >  P_b$) peut être
écrite en termes de volume relatif, par la relation:
$$C_o = -\frac{1}{V_{rel}} \frac{\partial V_{rel}}{\partial P}$$
Les résultats de ce test (échantillon du puits RDC1B) sont présentés
dans le \cref{tab:1110}.

%%%\includegraphics[width=3.16442in,height=2.40000in]{part2/chapter2/chapter2Media2/media/image3.emf}
\begin{table}[t]
    \centering
    \begin{tabular}{clllll}
    \toprule
       &  Pression (Psi)& Volume (cm$^3$)& V/Vsat (m$^{3}$.m$^{-3}$)& Densite (g.cm$^3$)&
       Co (1e-6 Psi$^{-1}$)\\
       \cmidrule(lr){2-6}
       &  6000 & 25.75 & 0.9502 & 0.637 & 11.65 \\
       &  5000 & 26.12 & 0.9638 & 0.628 & 15.31 \\
       &  4500 & 26.34 & 0.972 & 0.623 & 17.08 \\
       &  4000 & 26.55 & 0.9797 & 0.618 & 18.83 \\
       &  3500 & 26.84 & 0.9904 & 0.611 & 20.49 \\
       &  3200 & 27.02 & 0.997 & 0.607 & 21.47 \\
        Psat&  3040 & 27.1 & 1 & 0.605 & 21.99 \\
            &  2500 & 29.17 & 1.0764 & & \\
            &  2200 & 32.68 & 1.2059 & & \\
            &  1500 & 39.18 & 1.4458 & & \\
            &  1000 & 53.48 & 1.9734 & & \\
    \bottomrule
    \end{tabular}
    \caption{Résultas du test CCE}
    \label{tab:1110}
\end{table}

\begin{figure}[!b]
    \centering
    \begin{center}
    \includegraphics[width=0.6\textwidth]{part2/chapter2/chapter2Media2/media/image04.png}
    \end{center}
    \caption{Expansion à composition constante}
    \label{im:1118}
\end{figure}

Il convient de noter qu'aucun hydrocarbure n'est éliminé. Ainsi, la
composition du mélange d'hydrocarbures dans la cellule reste fixée à la
composition d'origine.

\subsubsection{Libération différentielle (DL)}\label{liberation-differentielle-dl}

l'objectif de ce test est de trouver les valeurs de Bo, Rs, la viscosité
la densité de l'huile, les Propriétés du gaz dégagé, y compris la
composition du gaz libéré, le facteur de compressibilité du gaz et sa
densité en fonction de la pression.

Le test est effectué sur des échantillons d'huile de réservoir dans une
cellule PVT à la pression de bulle et à la température du réservoir.
Comme le montre schématiquement la \cref{im:1119}, la pression est
réduite par paliers, généralement entre 10 et 15 niveaux de pression
jusqu'à la pression atmosphérique, et tout le gaz libéré est éliminé et
son volume est mesuré dans les conditions standard. Le volume d'huile
restant $V_L$ est également mesuré à chaque niveau de pression. Il
convient de noter que le pétrole restant est soumis à des changements
continus de composition alors qu'il s'enrichit progressivement en
composants plus lourds.Le volume de l'huile résiduelle (restante) est
mesuré à 60 ° F et $P_{atm}$, $V_{sc}$. Les facteurs de
volume de formation d'huile obtenus par la vaporisation différentielle
$B_{od}$
est calculé par la relation suivante:
\begin{ceqn}\begin{align}B_{od} = \frac{V_L}{V_{sc}}\end{align}\end{ceqn}

Les autres paramètres:
\begin{itemize}
\item
$Z= \frac{V_gP}{T}\frac{T_{sc}}{V_{gsc}P_{sc}}$ le facteur de compressibilité du gaz
\item
$V_g$, volume du gaz libéré dans la cellule PVT en P et T
\item
$V_{gsc}$, volume du gaz éliminé aux conditions standards
\item
$B_{g} = \frac{P_{sc}}{T_{sc}}\frac{ZT}{P}$ le facteur de volume de formation du gaz
\item
$T$ la température, °R
\item
$P$ la pression cellulaire, psia
\item
$T_{sc}$ = température standard, °R
\item
$P_{sc}$ = pression standard, psi
\end{itemize}

\begin{figure}[!b]
    \centering
    \begin{center}
    \includegraphics[width=0.6\textwidth]{part2/chapter2/chapter2Media2/media/image8.png}
    \end{center}
    \caption{Vaporisation différentielle}
    \label{im:1119}
\end{figure}

\begin{tcbremark}[colbacktitle=green,remember as=one]{}
Il convient de noter que le test de libération
différentielle représente le comportement de l'huile dans le
réservoir lorsque la pression diminue. Nous devons trouver un
moyen de ramener cette huile à la surface à travers des séparateurs et
dans le bac de stockage. Ce processus est un processus flash ou
séparateur.
\end{tcbremark}

Les résultats de ce test sont présentés dans le \cref{tab:1111}.

%%%\includegraphics[width=5.76806in,height=2.20975in]{part2/chapter2/chapter2Media2/media/image9.emf}
\begin{table}[t]
    \centering
    \begin{tabular}{clllllllll}
    \toprule
& Pressure & Bo             & $\rho_o$ & $\mu_o$ & Bg             & $\mu_g$ & Gas gravity
& Z & GOR dissous \\
& Psia     & m$^3$.m$^{-3}$ & g.cm$^3$ & cp      & m$^3$.m$^{-3}$ & cp      & & &
(m$^3$.m$^{-3}$)\\
       \cmidrule(lr){2-10} 
        $P_{sat}$ & 3040 & 1.7477 & 0.605 & 0.236 &       &         &       &       & 196.95\\
                  & 2500 & 1.6166 & 0.616 & 0.28  & 0.681 & -0.0213 &       & 0.858 & 140.38 \\
                  & 2000 & 1.4816 & 0.648 & 0.329 & 0.853 & 0.0188  &       & 0.862 & 106.26 \\
                  & 1500 & 1.3617 & 0.684 & 0.393 & 1.15 & 0.0164 &         & 0.872 & 79.49 \\
                  & 1000 & 1.2683 & 0.715 & 0.472 & 1.762 & 0.0154 &        & 0.89  & 56.43 \\
                  & 500  & 1.1477 & 0.765 & 0.553 & 3.634 & 0.014 &         & 0.917 & 32.83 \\
                  & 14.7 & 1      & 0.818 &       &       &       &  0.772  & 1 & 0\\
    \bottomrule
    \end{tabular}
    \caption{Résultats du test DL (RDC1B, $T_g$ = 115 \textdegree C)}
    \label{tab:1111}
\end{table}


\subsubsection{Test de séparateur (TS)}\label{test-de-separateur-ts}

L'objectif principal des essais de séparation est de fournir les
informations de laboratoire nécessaires pour déterminer les conditions
optimales de séparation de surface, ce qui optimisera la production de
pétrole dans les bacs de stockage~;la procédure de laboratoire est
répétée en variant dans cahque test la pression des séparateur à une
température fixe. Il est généralement recommandé d'utiliser quatre de
ces tests pour déterminer la pression optimale du séparateur, qui est
généralement considérée comme la pression du séparateur entraînant un
facteur de volume de formation d'huile minimal. À la même pression, la
gravité de l'huile du réservoir sera maximale et le gaz total dégagé,
c'est-à-dire le gaz séparateur et le gaz du réservoir, sera au minimum,
la separation est d'autant mieux que le gaz est moins
libéré(malheureusement on a pas eu ces données). En outre, les résultats
du test, lorsqu'ils sont combinés de manière appropriée avec les données
du test de libération différentielle, permettent d'obtenir les
paramètres PVT (Bo, Rs et Bt) nécessaires aux calculs
d'ingénierie pétrolière. Ces tests de séparation sont effectués
uniquement sur l'huile d'origine au point de bulle.

Le test consiste à placer un échantillon de brut à sa pression de
saturation et à la température du réservoir dans une cellule PVT. Le
volume de l'échantillon mesuré est $V_{sat}$. L'échantillon
d'hydrocarbures est ensuite passé au flash dans un système de séparation
multi-étagée, généralement un à trois étages. La pression et la
température de ces étages sont définies pour représenter les
installations de séparation de surface. Le gaz libéré de chaque étage
est éliminé et sa densité et son volume dans les conditions standard
sont mesurés. Le volume de l'huile restante dans la dernière étape
(représentant l'état du stock tank) est mesuré et enregistré comme
$V_{ost}$. Ces données mesurées expérimentales peuvent ensuite
être utilisées pour déterminer le facteur de volume de formation d'huile
et
la solubilité du gaz à la pression de bulle comme suit:
\begin{ceqn}\begin{align} B_{ofb}=\frac{V_{sat}}{V_{ost}}, \qquad
R_{sfb}=\frac{V_{gsc}}{V_{ost}} \end{align}\end{ceqn}

\begin{itemize}
    \item 
        $B_{ofb}$, facteur de volume de formation d'huile au point de bulle, mesuré par la libération flash.
    \item 
        $R_{sfb}$, rapport gas en solution-huile au point de bulle mesuré par éclair la libération flash
    \item 
        $V_{gsc}$, volume total de gaz extrait des séparateurs.
\end{itemize}

Les résultats de l'essai de séparation sont montrés dans le \cref{tab:septest}.
\begin{table}[!b]
$$\begin{array}{|l|l|}\hline \text { GOR-flash($R_{sfb}$) } & {187 \quad
    \text{m}^3.\text{m}^{-3}}
    \\ \hline \text { FVF ($B_{ofb}$) } & {1.6751 \quad \text{m}^3.\text{m}^{-3}} \\ \hline
\rho_o & {0.8137 \quad\text{g}.\text{cm}^{-3}} \\ \hline\end{array}$$
    \caption{Résultats du test de séparation}
    \label{tab:septest}
\end{table}

\subsection{Commentaire des résultats}\label{commentaire-des-resultats}

Les données de libération différentielle montrent que le rapport
Rs est de 196.95
m$^{3}$.m$^{-3}$ (Bo est de
1.7477 m$^{3}$.m$^{-3}$) au point de bulle
par rapport à 187 m$^{3}$.m$^{-3}$
(1.6751 m$^{3}$.m$^{-3}$); valeurs obtenues
du test du séparateur. Cette différence est attribuée au fait que les
processus d'obtention de l'huile résiduelle Vsc (DL) et de l'huile du
stock-tank (Vo)st à partir d'huile de point de bulle sont différents:
La libération différentielle est considérée comme une série
multiple de flashes
aux températures élevées du réservoir. Le test du séparateur
consiste généralement en un flash à un ou deux étages à basse
pression et à basse température. La quantité de gaz libérée
sera différente et la quantité de liquide final sera différente.

\subsection{Ajustement
des données de libération différentielle aux conditions du séparateur}

Pour pouvoir calculer le bilan matière, il faut obtenir le facteur de
volume de formation d'huile, Bo, et la solubilité du gaz, en fonction de
la pression du réservoir. \autocite{amyx1960petroleum} et \autocite{dake1978fundamentals} ont proposé
une procédure pour construire les courbes de facteur de volume de
formation de l'huile et de solubilité du les gaz en utilisant les
données de libération différentielle (\cref{tab:1111}) en conjonction
avec les données de séparation expérimentale du séparateur
(taleau 3) .La méthode est résumée comme suit:
\begin{ceqn}\begin{align} B_o=\left(\frac{B_{ofb}}{B_{odb}}\right) B_{od}, \quad \text {
si } \quad P \leq P_b \end{align}\end{ceqn} 
\begin{itemize}
    \item 
        $B_{ofb}$, le facteur de volume de formation d'huile au point de bulle, mesuré
par la libération flash (TS).
\item 
    $B_{odb}$, facteur de volume de formation d'huile au point de bulle, mesuré par
la libération différentielle (DL).
\item 
    $B_{od}$, facteur de volume de formation d'huile, mesuré par la libération
différentielle.
\end{itemize}
\begin{ceqn}\begin{align} B_o = B_{ofb} \mathrm{V}_{rel}, \quad \text { si } \quad P > P_b
\end{align}\end{ceqn}

$V_{rel}$ est le volume relatif mesuré par
l'expansion de composition constante (CCE) (\cref{tab:1110}).

\begin{ceqn}\begin{align}
Rs = R_{sfb}-(R_{sdb}-R_{sd})\left(\frac{B_{ofb}}{B_{odb}}\right)
\end{align}\end{ceqn}
\begin{itemize}
    \item 
        $R_{sfb}$ la solubilité du gaz au point de bulle, mesurée par la libération
flash (TS).
    \item 
        $R_{sdb}$ la solubilité du gaz au point de bulle, mesurée par la libération
différentielle (DL).
    \item 
        $R_{sd}$ la solubilité du gaz au point de bulle, mesurée par la libération
différentielle.
\end{itemize}

Les résultats PVT avec les paramètres corrigés (Rs,Bo) sont présentés
dans le \cref{tab:1112}.

\begin{table}[t]
    \centering
    \begin{tabular}{cllllllll}
        \toprule
    & Pressure & Bo(corrected) & Rs(corrected) & $\mu_o$ & Bg & $\mu_g$ & Gas gravity & Z \\
& Psia     & m$^3$.m$^{-3}$ & m$^3$.m$^{-3}$ & cp      & m$^3$.m$^{-3}$ & cp      & & \\
   \cmidrule(lr){2-9} 
   & {6000} & {1.59225014} & {187} & {0.295} & & & & \\ 
   & {5000} & {1.61503966} & {187} & {0.275} & & & & \\
   & {4500} & {1.6287804} & {187} & {0.266} & & & & \\ 
   & {4000} & {1.64168329} & {187} & {0.256} & & & & \\
   & {3500} & {1.65961328} & {187} & {0.246} & & & & \\ 
   & {3200} & {1.6706729} & {187} & {0.239} & & & & \\
        $P_{sat}$ & {3040} & {1.6757} & {187} & {0.236} & & & & \\
   & {2500} & {1.550000927} & {132.7605144} & {0,28} & {0,681} & {0,0213} &  & {0,858} \\ 
   & {2000} & {1.420562522} & {100.0461561} & {0,329} & {0,853} & {0,0188} & & {0,862} \\ 
   & {1500} & {1.305602043} & {74.37899983} & {0,393} & {1,15} & {0,0164} & & {0,872} \\ 
   & {1000} & {1.216049843} & {52.26900269} & {0,472} & {1,762} & {0,0154} & & {0,89} \\ 
   & {500} & {1.100418201} & {29.64125193} & {0,553} & {3,634} & {0,014} & & {0,917} \\
   & {14.7} & {0.958802998} & {0} & & & & 0.772 & 1\\
       \bottomrule
    \end{tabular}
    \caption{Résultats PVT finaux}
    \label{tab:1112}
\end{table}

Le GOR maximal au début de l'exploitation du réservoir GRDC est de : 187 m$^3$.m$^{-3}$
(1049 SCF/STB). D'après la classification du brut par le GOR, on peut dire que l'huile
du réservoir GRDC est de type Black Oil.

\begin{minipage}{\textwidth}
  \begin{minipage}[!b]{0.49\textwidth}
    \centering
\begin{figure}[H]
    \centering
    \begin{center}
    \includegraphics[width=\textwidth,height=0.16\textheight]{part2/chapter2/chapter2Media2/media/image17.png}
    \end{center}
    \caption{Evolution du $B_o$ en fonction de la pression}
    \label{im:1120}
\end{figure}
  \end{minipage}
  \hfill
  \begin{minipage}[!b]{0.49\textwidth}
    \centering
\begin{table}[H]
    \centering
    \begin{tabular}{ll}
\toprule
Fluide & GOR\tabularnewline
   \cmidrule(lr){1-1} \cmidrule(lr){2-2}
Black Oils & < 1800 scf/stb\tabularnewline
Volatile Oils & 1800 - 3300 scf/stb\tabularnewline
Gas Condensates & 3300 - 50 000 scf/stb\tabularnewline
Wet Gases & 50 000 - 100 000 scf/stb\tabularnewline
Dry Gases & > 100 000 scf/stb\tabularnewline
\bottomrule
    \end{tabular}
    \caption{Classification des fluides de réservoir par GOR}
    \label{tab:1113}
\end{table}
    \end{minipage}
  \end{minipage}


\begin{sidewaysfigure}
  \centering
  \begin{subfigure}{.49\textheight}
    \includegraphics[width=\textwidth,height=0.3\textheight]{part2/chapter2/chapter2Media2/media/image18.png}
    \caption{ Evolution du $R_s$ en fonction de la pression}
    \label{im:1121}
  \end{subfigure}~
  \begin{subfigure}{.49\textheight}
    \includegraphics[width=\textwidth,height=0.3\textheight]{part2/chapter2/chapter2Media2/media/image19a.png}
    \caption{ Evolution du $B_g$ en fonction de la pression}
    \label{im:1122}
  \end{subfigure}
  \begin{subfigure}{.49\textheight}
    \includegraphics[width=\textwidth,height=0.3\textheight]{part2/chapter2/chapter2Media2/media/image20a.png}
    \caption{ Evolution du $\mu_o$ en fonction de la pression}
    \label{im:1123}
  \end{subfigure}
  \begin{subfigure}{.49\textheight}
    \includegraphics[width=\textwidth,height=0.3\textheight]{part2/chapter2/chapter2Media2/media/image21a.png}
    \caption{ Evolution du $\mu_g$ en fonction de la pression}
    \label{im:1124}
  \end{subfigure}
  \caption{Evolution des paramètres $R_s$, $B_g$, $\mu_o$  \& $\mu_g$ en fonction de la pression}
\end{sidewaysfigure}
%%%\includegraphics[width=5.76806in,height=3.23426in]{part2/chapter2/chapter2Media2/media/image17.emf}
