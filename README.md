# Master-Thesis

This repo holds Latex sources for the written thesis and the presentation file which are
published under the LPPL version 1.3c. If it's the first time you hear about Latex, you should
start learning about it [here](https://en.wikibooks.org/wiki/LaTeX)

- If you have any concerns/suggestions, use the issues system
- This was typeset with LuaLatex through `latexmk` with special treatment for 
  citations and math symbol definitions.
- The code is not organized; No, It's a mess, if you need assistance, open up an
  issue.
- If you only care for the thesis and nothing else, download the `main.pdf` file 
  ( The presentation is called `demo.pdf`).

# What do I need to make this work as expected

- Install `LuaLatex` engine for your platform.
- Install and configure `latexmk` for automatic builds. On Unix systems, your
  `~/.latexmkrc` file should look like this:
```bash
# latexmk config file to use LuaLatex
$pdflatex = 'lualatex -shell-escape -enable-write18 -file-line-error %O %S';
$pdf_mode = 1;
# Custom dependency for nomencl package & bibliography
 add_cus_dep( 'nlo', 'nls', 0, 'makenlo2nls' );
 sub makenlo2nls {
 system( "makeindex -s nomencl.ist -o \"$_[0].nls\" \"$_[0].nlo\"" );
 }
```

# How to build the PDF files

- In a command prompt from the repo root folder: `cd presentation/demo; latexmk demo.tex` 
  builds the presentation file (recommended viewer: Adobe Reader)
- `cd thesis; latexmk main.tex` builds the printable thesis (You may choose to remove some
  chapters while experimenting by commenting out `\input{part/chapter/section.tex}` lines.
